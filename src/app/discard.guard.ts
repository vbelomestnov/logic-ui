import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscriber, of } from 'rxjs';

export interface CanComponentDeactivate {
  presentAlertConfirm: () => Promise<boolean>;
}

const URL_GROUP = [
  '/editor/',
  '/shape/new',
  '/link/new',
  '/shape/edit/',
];

@Injectable({
  providedIn: 'root'
})
export class DiscardGuard implements CanDeactivate<CanComponentDeactivate> {
  canDeactivate(
    component: CanComponentDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot) {
    const groupContainCurrent = !!URL_GROUP.find((url) => currentState.url.includes(url));
    const groupContainNext = !!URL_GROUP.find((url) => nextState.url.includes(url));
    if (groupContainCurrent && groupContainNext) {
      return of(true);
    } else {
      return new Observable((subscriber: Subscriber<boolean>) => {
        component.presentAlertConfirm()
          .then((result) => {
            subscriber.next(result);
          });
      });
    }
  }
}
