import { InjectionToken } from '@angular/core';
import { AppPlugin } from '@capacitor/core';
export const APP_PLUGIN = new InjectionToken<AppPlugin>('AppPlugin');
