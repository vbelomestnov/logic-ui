import { InjectionToken } from '@angular/core';

export interface VkOpenApi {
    login: () => {};
    sharePost: (args: { message: string; link: string }) => {};
}
export const VK_OPEN_API = new InjectionToken<VkOpenApi>('VkOpenApi');
