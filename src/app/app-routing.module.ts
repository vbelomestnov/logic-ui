import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'editor/:code', loadChildren: './pages/editor/editor.module#EditorPageModule' },
  { path: 'shape/new', loadChildren: './pages/shape-edit/shape-edit.module#ShapeEditPageModule' },
  { path: 'shape/edit/:name', loadChildren: './pages/shape-edit/shape-edit.module#ShapeEditPageModule' },
  { path: 'shape/new/position', loadChildren: './pages/shape-new-position/shape-new-position.module#ShapeNewPositionPageModule' },
  { path: 'preview/:code', loadChildren: './pages/preview/preview.module#PreviewPageModule' },
  { path: 'link/new', loadChildren: './pages/link-new/link-new.module#LinkNewPageModule' },
  { path: 'search', loadChildren: './pages/search/search.module#SearchPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
