import { TestBed, async, inject } from '@angular/core/testing';

import { DiscardGuard, CanComponentDeactivate } from './discard.guard';

describe('DiscardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DiscardGuard]
    });
  });

  it('should call discard dialog', (done) => {
    inject([DiscardGuard], (guard: DiscardGuard) => {
      expect(guard).toBeTruthy();

      let mode = false;
      const component: CanComponentDeactivate = {
        presentAlertConfirm: async () => mode,
      };
      guard.canDeactivate(component, null, { url: '/editor/123', root: null, }, { url: '/shape/new', root: null }).subscribe(
        (result) => {
          expect(result).toBe(true);

          mode = true;
          guard.canDeactivate(component, null, { url: '123', root: null, }, { url: '456', root: null }).subscribe(
            (result2) => {
              expect(result).toBe(true);

              mode = false;
              guard.canDeactivate(component, null, { url: '123', root: null, }, { url: '456', root: null }).subscribe(
                (result3) => {
                  expect(result3).toBe(false);
                  done();
                });
            });
        });
    })();
  });

  it('should ignore discard if shape edit route is activated', (done) => {
    inject([DiscardGuard], (guard: DiscardGuard) => {
      expect(guard).toBeTruthy();

      const mode = false;
      const component: CanComponentDeactivate = {
        presentAlertConfirm: async () => mode,
      };
      guard.canDeactivate(component, null, { url: '/shape/edit/object-1', root: null, }, { url: '/shape/new', root: null }).subscribe(
        (result) => {
          expect(result).toBe(true);
          done();
        });
    })();
  });
});
