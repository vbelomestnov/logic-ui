import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { HelperService } from './services/helper.service';
import { GraphModule, APP_CONFIG, SocketTesting } from 'logic-common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Socket } from 'ngx-socket-io';
import { APP_PLUGIN } from './types/app-plugin';

describe('AppComponent', () => {

  let statusBarSpy, splashScreenSpy, platformReadySpy, platformSpy, fixture, router: Router, helperService: HelperService;

  beforeEach(async(() => {
    statusBarSpy = jasmine.createSpyObj('StatusBar', ['styleDefault']);
    splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });
    platformSpy.backButton = new Subject();
    platformSpy.resume = new Subject();

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, GraphModule, HttpClientTestingModule],
      providers: [
        { provide: StatusBar, useValue: statusBarSpy },
        { provide: SplashScreen, useValue: splashScreenSpy },
        { provide: Platform, useValue: platformSpy },
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .overrideComponent(
        AppComponent,
        { set: { providers: [{ provide: APP_PLUGIN, useValue: MockAppPlugin }] } }
      )
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    router = TestBed.get(Router);
    helperService = TestBed.get(HelperService);
  });

  it('should create the app', () => {
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(platformSpy.backButton.observers.length).toBe(1);
    if (platformSpy.backButton.observers[0]) {
      platformSpy.backButton.observers[0].complete();
    }

    platformSpy.backButton.subscribe(() => { });
    const extraFixture = TestBed.createComponent(AppComponent);
    extraFixture.detectChanges();
    expect(platformSpy.backButton.observers.length).toBe(1);
  });

  it('should initialize the app', async () => {
    expect(platformSpy.ready).toHaveBeenCalled();
    await platformReadySpy;
    expect(statusBarSpy.styleDefault).toHaveBeenCalled();
    expect(splashScreenSpy.hide).toHaveBeenCalled();
  });

  xit('shoule release exitService subscriotions when NavigationStart', async () => {
    fixture.detectChanges();
    spyOn(helperService, 'releaseSubscription');
    const spy = helperService.releaseSubscription as jasmine.Spy;
    router.navigate(['/']);
    expect(spy).toHaveBeenCalled();
  });

  it('should exit on second tap on native back button', async () => {
    fixture.detectChanges();
    navigator['app'] = { exitApp: () => { } };
    spyOn(navigator['app'], 'exitApp');
    const spy = navigator['app'].exitApp as jasmine.Spy;
    platformSpy.backButton.next();
    expect(spy).not.toHaveBeenCalled();
    helperService.canExit = true;
    platformSpy.backButton.next();
    expect(spy).toHaveBeenCalled();
  });

  it('should reset exit state when tap on "Tap back again for exit the application"', async () => {
    fixture.detectChanges();
    const component = fixture.componentInstance;
    helperService.canExit = true;
    component.clickHandler();
    expect(helperService.canExit).toBe(false);
  });

  it('should unsubscribe back button on destroy component', async () => {
    fixture.detectChanges();
    const observers = platformSpy.backButton.observers.length;
    fixture.destroy();
    expect(platformSpy.backButton.observers.length).toBeLessThan(observers);
  });

  it('should close loading spinner on "platform.resume"', fakeAsync(() => {
    helperService.hideLoading = () => Promise.resolve();
    fixture.detectChanges();
    helperService.publicationInProgress = true;
    platformSpy.resume.next();
    tick();
    expect(helperService.publicationInProgress).toBe(false);

    const spy = spyOn(helperService, 'hideLoading');
    platformSpy.resume.next();
    tick();
    expect(helperService.publicationInProgress).toBe(false);
    expect(spy).not.toHaveBeenCalled();
  }));

  it('should handle deep link', fakeAsync(() => {
    fixture.detectChanges();
    tick();
    if (!appUrlOpenCallback) {
      fail('Unexpected behavior');
    } else {
      const spyNavigate = spyOn(router, 'navigate');
      appUrlOpenCallback({ url: 'https://impacton.world/graph/123' });
      expect(spyNavigate).toHaveBeenCalled();
      const arg = spyNavigate.calls.mostRecent().args[0];
      expect(arg instanceof Array).toBe(true);
      expect(arg[0]).toBe('preview');
      expect(arg[1]).toBe('123');
    }
  }));

});

let appUrlOpenCallback = null;
// tslint:disable-next-line:variable-name
const MockAppPlugin = {
  addListener: (event, callback) => {
    appUrlOpenCallback = callback;
  },
};
