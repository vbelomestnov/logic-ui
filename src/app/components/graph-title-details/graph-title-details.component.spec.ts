import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphTitleDetailsComponent } from './graph-title-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GraphService, graphStubData, APP_CONFIG, SocketTesting } from 'logic-common';
import { Socket } from 'ngx-socket-io';

describe('GraphTitleDetailsComponent', () => {
  let component: GraphTitleDetailsComponent;
  let fixture: ComponentFixture<GraphTitleDetailsComponent>;
  let graphService: GraphService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GraphTitleDetailsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: APP_CONFIG, useValue: {}},
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();

    fixture = TestBed.createComponent(GraphTitleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle Edit state only is not in readonly mode', () => {
    component.readonly = true;
    expect(component.isEdit).toBe(false);
    component.toggleEdit();
    expect(component.isEdit).toBe(false);
    component.readonly = false;
    component.toggleEdit();
    expect(component.isEdit).toBe(true);
  });

  it('should redo should restore old text value', () => {
    component.title = 'old value';
    component.editedTitle = 'new value';
    component.isEdit = true;
    component.redo();
    expect(component.editedTitle).toBe(component.title);
    expect(component.isEdit).toBe(false);
  });

  it('should save title in the graph service and go back to the graph screen', () => {
    component.goBack = () => {};
    const newName = 'New name of graph';
    spyOn(component, 'goBack');
    const spy = component.goBack as jasmine.Spy;
    const saveSpy = spyOn(graphService, 'saveTitle');
    component.saveHandler(newName);
    expect(spy).toHaveBeenCalled();
    expect(saveSpy).toHaveBeenCalled();
  });
});
