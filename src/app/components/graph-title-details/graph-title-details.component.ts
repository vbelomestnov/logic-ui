import { Component, OnInit, Input, } from '@angular/core';
import { GraphService } from 'logic-common';

@Component({
  selector: 'app-graph-title-details',
  templateUrl: './graph-title-details.component.html',
  styleUrls: ['./graph-title-details.component.scss'],
})
export class GraphTitleDetailsComponent implements OnInit {

  @Input() title: string;
  public editedTitle: string;
  @Input() goBack;
  @Input() readonly = false;

  public isEdit = false;

  constructor(
    private graphService: GraphService,
  ) { }

  ngOnInit() {
    this.editedTitle = this.title;
  }

  toggleEdit(): void {
    if (this.readonly) {
      return;
    }
    this.isEdit = !this.isEdit;
  }

  redo(): void {
    this.editedTitle = this.title;
    this.toggleEdit();
  }

  saveHandler(title: string): void {
    this.graphService.saveTitle(title);
    this.goBack();
  }
}
