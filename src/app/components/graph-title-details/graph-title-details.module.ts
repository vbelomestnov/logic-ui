import { NgModule } from '@angular/core';
import { GraphTitleDetailsComponent } from './graph-title-details.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderModule } from '../header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderModule,
  ],
  declarations: [
    GraphTitleDetailsComponent,
  ],
  exports: [
    GraphTitleDetailsComponent,
  ],
  entryComponents: [
    GraphTitleDetailsComponent,
  ]
})
export class GraphTitleDetailsModule {}
