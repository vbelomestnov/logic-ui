import { Component, OnInit, ContentChild, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @ContentChild('headerButtons', { static: true }) headerButtonsTemplate: TemplateRef<any>;

  constructor() { }

  ngOnInit() { }

}
