import { Component, OnDestroy, OnInit, Inject } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { trigger, style, animate, transition } from '@angular/animations';
import { Router } from '@angular/router';
import { HelperService } from './services/helper.service';
import { APP_PLUGIN } from './types/app-plugin';
import { AppPlugin, AppUrlOpen } from '@capacitor/core';
import { Plugins } from '@capacitor/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('500ms ease', style({ opacity: '*' }))
      ]),
      transition(':leave', animate('500ms ease', style({ opacity: '0' }))),
    ]),
  ],
  providers: [
    { provide: APP_PLUGIN, useValue: Plugins.App },
  ]
})
export class AppComponent implements OnDestroy, OnInit {

  backButtonSubscription;

  constructor(
    @Inject(APP_PLUGIN) private appPlugin: AppPlugin,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public helperService: HelperService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    // Complete default handle of backButton's event
    if (this.platform.backButton.observers[0]) {
      this.platform.backButton.observers[0].complete();
    }
    // Code excluded due to discard dialog issues
    // this.router.events.pipe(
    //   filter(event => event instanceof NavigationStart)
    // )
    //   .subscribe(() => {
    //     this.helperService.releaseSubscription();
    //   });
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      if (this.helperService.canExit) {
        navigator['app'].exitApp();
      }
    });

    this.platform.resume.subscribe(async () => {
      if (this.helperService.publicationInProgress) {
        await this.helperService.hideLoading();
        this.helperService.publicationInProgress = false;
      }
    });

    this.appPlugin.addListener('appUrlOpen', (urlOpen: AppUrlOpen) => {
      const splittedUrl = urlOpen.url.split('/');
      const code = splittedUrl[splittedUrl.length - 1];
      this.router.navigate(['preview', code]);
    });
  }

  ngOnDestroy() {
    if (this.backButtonSubscription) {
      this.backButtonSubscription.unsubscribe();
    }
  }

  clickHandler() {
    this.helperService.canExit = false;
  }
}
