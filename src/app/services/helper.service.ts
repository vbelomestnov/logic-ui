import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';
import { GraphService } from 'logic-common';
import { GraphError } from 'projects/logic-common/src/public-api';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  public canExit = false;
  private canDismiss = false;
  private _backButtonSubscription: Subscription = null;
  set backButtonSubscription(value: Subscription) {
    if (this._backButtonSubscription) {
      this._backButtonSubscription.unsubscribe();
    }
    this._backButtonSubscription = value;
  }
  private loading: HTMLIonLoadingElement = null;
  public publicationInProgress = false;

  constructor(
    private toastController: ToastController,
    private graphService: GraphService,
    private alertController: AlertController,
    private loadingController: LoadingController,
  ) { }

  releaseSubscription() {
    if (this._backButtonSubscription) {
      this._backButtonSubscription.unsubscribe();
      this._backButtonSubscription = null;
    }
    this.canExit = false;
  }

  async presentErrorToast(message: string | GraphError[]) {
    let messages: string[] = [];
    if (typeof message === 'string') {
      messages.push(message);
    } else {
      messages = this.graphService.extractGraphErrors(message);
    }
    this.presentToast(messages.toString().split(',').join('\n'), 'danger');
  }

  async presentSuccessToast() {
    await this.presentToast('Success', 'success');
  }

  private async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message,
      duration: 10000,
      color,
      showCloseButton: true,
      closeButtonText: 'X',
    });
    toast.present();
  }

  async presentDismissAlertConfirm() {
    this.canDismiss = false;
    if (!this.graphService.hasChanged) {
      return Promise.resolve(true);
    } else {
      const alert = await this.alertController.create({
        header: 'Discard changes',
        message: 'Are you sure?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
          }, {
            text: 'Okay',
            handler: () => {
              this.canDismiss = true;
            }
          }
        ]
      });
      const result = new Promise<boolean>((resolve) => {
        alert.onDidDismiss()
          .then((res) => {
            resolve(this.canDismiss);
          });
      });
      await alert.present();
      return result;
    }
  }

  public async showLoading() {
    await this.hideLoading();
    this.loading = await this.loadingController.create({});
    await this.loading.present();
  }

  public async hideLoading() {
    const top = await this.loadingController.getTop();
    if (top) {
      await this.loadingController.dismiss();
    }
  }
}
