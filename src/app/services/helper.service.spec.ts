import { TestBed } from '@angular/core/testing';

import { HelperService } from './helper.service';
import { Subject } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SocketTesting, GraphService, APP_CONFIG, graphStubData } from 'logic-common';
import { Socket } from 'ngx-socket-io';
import { AlertController } from '@ionic/angular';

describe('HelperService', () => {
  let service: HelperService;
  let graphService: GraphService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
        { provide: AlertController, useClass: AlertControllerTesting },
      ]
    });
    service = TestBed.get(HelperService);
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('sould unsibscribe backButtonSubscription if new subscription is requested', () => {
    const subj = new Subject();
    const subscrption = subj.subscribe();
    expect(subj.observers.length).toBe(1);
    service.backButtonSubscription = subscrption;
    const subscrption2 = subj.subscribe();
    service.backButtonSubscription = subscrption;
    expect(subj.observers.length).toBe(1);
  });

  it('sould release subscription', () => {
    const subj = new Subject();
    const subscrption = subj.subscribe();
    expect(subj.observers.length).toBe(1);
    service.backButtonSubscription = subscrption;
    service.canExit = true;
    service.releaseSubscription();
    expect(subj.observers.length).toBe(0);
    expect(service.canExit).toBe(false);
    service.canExit = true;
    service.releaseSubscription();
    expect(service.canExit).toBe(false);
  });

  it('should present discard confirm alert', async () => {

    ALERT_CONTROLLER_MODE = true;
    expect(await service.presentDismissAlertConfirm()).toBe(true);

    ALERT_CONTROLLER_MODE = false;
    expect(await service.presentDismissAlertConfirm()).toBe(false);

    graphService.newGraph();
    ALERT_CONTROLLER_MODE = false;
    expect(await service.presentDismissAlertConfirm()).toBe(true);
  });
});

let ALERT_CONTROLLER_MODE = false;
class AlertControllerTesting {
  data = null;
  mode = false;
  async create(data) {
    this.data = data;
    if (ALERT_CONTROLLER_MODE) {
      data.buttons[1].handler();
    }
    return {
      present: async () => { },
      onDidDismiss: async () => {
        return ALERT_CONTROLLER_MODE;
      },
    };
  }
}