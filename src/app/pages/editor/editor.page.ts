import { Component, OnInit, ViewChild, } from '@angular/core';
import { GraphData } from 'logic-common';
import { GraphService } from 'logic-common';
import { IonNav, NavController, Platform } from '@ionic/angular';
import { CanBackPage } from '../can-back.page';
import { Router, ActivatedRoute, } from '@angular/router';
import { GraphTitleDetailsComponent } from 'src/app/components/graph-title-details/graph-title-details.component';
import { HelperService } from '../../services/helper.service';
import { EditorMainComponent } from './editor-main/editor-main.component';
import { EditorShapeMenuComponent } from './editor-shape-menu/editor-shape-menu.component';
import { EditorLinkMenuComponent } from './editor-link-menu/editor-link-menu.component';
import { HttpErrorResponse } from '@angular/common/http';
import { Shape } from 'logic-common';
import { Link } from 'logic-common';
import { CanComponentDeactivate } from 'src/app/discard.guard';

@Component({
  selector: 'app-editor',
  templateUrl: 'editor.page.html',
  styleUrls: ['editor.page.scss'],
})
export class EditorPage extends CanBackPage implements OnInit, CanComponentDeactivate {

  root = EditorMainComponent;
  rootParams = null;

  @ViewChild(IonNav, { static: true }) nav: IonNav;

  constructor(
    private graphService: GraphService,
    protected helperService: HelperService,
    protected navCtrl: NavController,
    protected platform: Platform,
    protected router: Router,
    private route: ActivatedRoute,
  ) {
    super(navCtrl, platform, router, helperService);
    this.pushShapeMenu = this.pushShapeMenu.bind(this);
    this.pushLinkMenu = this.pushLinkMenu.bind(this);
    this.pushGraphTitleDetails = this.pushGraphTitleDetails.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  ngOnInit() {
    this.rootParams = {
      data: this.graphService.data,
      pushShapeMenu: this.pushShapeMenu,
      pushLinkMenu: this.pushLinkMenu,
      pushGraphTitleDetails: this.pushGraphTitleDetails,
      goBack: this.goBack,
    };
    super.ngOnInit();
    this.route.params.subscribe(
      async params => {
        const code = params.code;
        if (code && code.length) {
          if (this.graphService.data && this.graphService.data.code === code ||
            (code === 'new' && this.graphService.data && this.graphService.data.code === '')) {
            await this.pushMain();
          } else if (code === 'new') {
            this.graphService.newGraph();
            await this.pushMain();
          } else {
            await this.helperService.showLoading();
            this.graphService.loadGraph(code)
              .subscribe(
                async (result: GraphData) => {
                  await this.pushMain();
                  this.helperService.hideLoading();
                },
                (error: HttpErrorResponse) => {
                  let message = error.statusText;
                  if (error.error && error.error.message) {
                    message = error.error.message;
                  }
                  this.helperService.presentErrorToast(message);
                  this.router.navigate(['/search']);
                  this.helperService.hideLoading();
                }
              );
          }
        } else {
          this.router.navigate(['/search']);
        }
      }
    );
  }

  async pushMain() {
    this.rootParams = {
      ...this.rootParams
    };
    await this.nav.setRoot(EditorMainComponent, this.rootParams);
    await this.nav.popToRoot();
  }

  pushShapeMenu(shape: Shape) {
    this.nav.push(EditorShapeMenuComponent, { shape, goBack: this.goBack });
  }

  pushLinkMenu(link: Link) {
    this.nav.push(EditorLinkMenuComponent, { link, goBack: this.goBack });
  }

  pushGraphTitleDetails(title: string) {
    this.nav.push(GraphTitleDetailsComponent, { title, goBack: this.goBack });
  }

  goBack() {
    this.nav.canGoBack()
      .then(
        can => {
          if (can) {
            this.nav.pop();
          } else {
            this.navCtrl.pop();
          }
        }
      );
  }

  presentAlertConfirm() {
    return this.helperService.presentDismissAlertConfirm();
  }

}
