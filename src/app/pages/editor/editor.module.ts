import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { GraphModule } from 'logic-common';
import { HeaderModule } from 'src/app/components/header/header.module';
import { GraphTitleDetailsModule } from 'src/app/components/graph-title-details/graph-title-details.module';
import { EditorMainComponent } from './editor-main/editor-main.component';
import { EditorShapeMenuComponent } from './editor-shape-menu/editor-shape-menu.component';
import { EditorLinkMenuComponent } from './editor-link-menu/editor-link-menu.component';
import { EditorPage } from './editor.page';
import { DiscardGuard } from 'src/app/discard.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: EditorPage,
        canDeactivate: [DiscardGuard]
      }
    ]),
    GraphModule,
    HeaderModule,
    GraphTitleDetailsModule,
  ],
  declarations: [EditorPage, EditorMainComponent, EditorShapeMenuComponent, EditorLinkMenuComponent],
  entryComponents: [EditorMainComponent, EditorShapeMenuComponent, EditorLinkMenuComponent],
})
export class EditorPageModule {}
