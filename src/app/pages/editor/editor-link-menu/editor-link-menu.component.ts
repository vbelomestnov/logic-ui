import { Component, OnInit, Input } from '@angular/core';
import { GraphService } from 'logic-common';
import { Link } from 'logic-common';

@Component({
  selector: 'app-editor-link-menu',
  templateUrl: './editor-link-menu.component.html',
  styleUrls: ['./editor-link-menu.component.scss'],
})
export class EditorLinkMenuComponent implements OnInit {

  @Input() link: Link;
  @Input() goBack;

  tapAgainForRemove = false;

  constructor(
    private graphService: GraphService,
  ) { }

  ngOnInit() { }

  remove() {
    if (this.link && this.tapAgainForRemove) {
      this.graphService.removeLink(this.link.source, this.link.dest);
      this.goBack();
      this.tapAgainForRemove = false;
    } else {
      this.tapAgainForRemove = true;
    }
  }

  getRemoveColor() {
    if (this.tapAgainForRemove) {
      return 'danger';
    } else {
      return '';
    }
  }
}
