import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { GraphService, graphStubData, APP_CONFIG, SocketTesting } from 'logic-common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EditorLinkMenuComponent } from './editor-link-menu.component';
import { Socket } from 'ngx-socket-io';

describe('EditorLinkMenuComponent', () => {
  let component: EditorLinkMenuComponent;
  let fixture: ComponentFixture<EditorLinkMenuComponent>;
  let graphService: GraphService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditorLinkMenuComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLinkMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('remove link', () => {
    const spyRemoveLink = spyOn(graphService, 'removeLink');
    const data = graphService.data;
    const link = data.links[0];
    component.link = link;
    component.goBack = () => { };
    expect(component.tapAgainForRemove).toBeFalsy();
    component.remove();
    expect(component.tapAgainForRemove).toBeTruthy();
    component.remove();
    expect(component.tapAgainForRemove).toBeFalsy();
    expect(spyRemoveLink).toHaveBeenCalled();
  });

  it('distinguish remove link by danger color', () => {
    const data = graphService.data;
    const link = data.links[0];
    component.link = link;
    component.goBack = () => { };
    expect(component.getRemoveColor()).toBe('');
    component.remove();
    expect(component.getRemoveColor()).toBe('danger');
  });
});
