import { Component, OnInit, Input } from '@angular/core';
import { GraphService } from 'logic-common';
import { Shape } from 'logic-common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editor-shape-menu',
  templateUrl: './editor-shape-menu.component.html',
  styleUrls: ['./editor-shape-menu.component.scss'],
})
export class EditorShapeMenuComponent implements OnInit {

  @Input() shape: Shape;
  @Input() goBack;

  tapAgainForRemove = false;

  constructor(
    private graphService: GraphService,
    private router: Router,
  ) { }

  ngOnInit() { }

  remove() {
    if (this.shape && this.tapAgainForRemove) {
      this.graphService.removeShape(this.shape.name);
      this.goBack();
      this.tapAgainForRemove = false;
    } else {
      this.tapAgainForRemove = true;
    }
  }

  toggleResizing() {
    this.graphService.toggleResizing(this.shape.name);
    this.goBack();
  }

  editText() {
    this.router.navigate(['/shape/edit/', this.shape.name]);
    this.goBack();
  }

  createLink(type) {
    this.graphService.queueNewLink(this.shape.name, type);
    this.router.navigate(['/link/new']);
    this.goBack();
  }

  getRemoveColor() {
    if (this.tapAgainForRemove) {
      return 'danger';
    } else {
      return '';
    }
  }

  getResizingColor() {
    if (this.shape && this.graphService.isShapeInResize(this.shape.name)) {
      return 'primary';
    } else {
      return '';
    }
  }
}
