import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { GraphService, graphStubData, APP_CONFIG, SocketTesting } from 'logic-common';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EditorShapeMenuComponent } from './editor-shape-menu.component';
import { Socket } from 'ngx-socket-io';

const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

describe('EditorShapeMenuComponent', () => {
  let component: EditorShapeMenuComponent;
  let fixture: ComponentFixture<EditorShapeMenuComponent>;
  let router: Router;
  let graphService: GraphService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditorShapeMenuComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        {
          provide: Router,
          useValue: routerSpy,
        },
        { provide: APP_CONFIG, useValue: {}},
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorShapeMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('remove shape', () => {
    const spyRemoveShape = spyOn(graphService, 'removeShape');
    const data = graphService.data;
    const shape = data.shapes[0];
    component.shape = shape;
    component.goBack = () => { };
    expect(component.tapAgainForRemove).toBeFalsy();
    component.remove();
    expect(component.tapAgainForRemove).toBeTruthy();
    component.remove();
    expect(component.tapAgainForRemove).toBeFalsy();
    expect(spyRemoveShape).toHaveBeenCalled();
  });

  it('toggle shape resize mode', () => {
    const data = graphService.data;
    const shape = data.shapes[0];
    component.shape = shape;
    component.goBack = () => { };
    expect(graphService.resizingShapeName).not.toBe(shape.name);
    component.toggleResizing();
    expect(graphService.resizingShapeName).toBe(shape.name);
    component.toggleResizing();
    expect(graphService.resizingShapeName).toBeNull();
  });

  it('edit text handler navigates to /shape/edit page', () => {
    const data = graphService.data;
    const shape = data.shapes[0];
    component.shape = shape;
    component.goBack = () => { };
    component.editText();
    const spy = router.navigate as jasmine.Spy;
    const navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs[0]).toBe('/shape/edit/');
    expect(navArgs[1]).toBe(shape.name);
    spy.calls.reset();
  });

  it('create link handler navigates to /link/new page', () => {
    const shape = graphService.data.shapes[0];
    component.shape = shape;
    component.goBack = () => { };

    expect(graphService.newLinks.length).toBe(0);
    component.createLink('source');
    const spy = router.navigate as jasmine.Spy;
    const navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs[0]).toBe('/link/new');
    expect(graphService.newLinks.length).toBe(1);
    expect(graphService.newLinks[0].source).toBe(shape.name);
    spy.calls.reset();
    graphService.refuseNewLinks();
  });

  it('distinguish remove link by danger color', () => {
    const data = graphService.data;
    const shape = data.shapes[0];
    component.shape = shape;
    component.goBack = () => {};
    expect(component.getRemoveColor()).toBe('');
    component.remove();
    expect(component.getRemoveColor()).toBe('danger');
  });

  it('distinguish move / resize link by primary color', () => {
    const data = graphService.data;
    const shape = data.shapes[0];
    component.shape = shape;
    component.goBack = () => {};

    expect(component.getResizingColor()).toBe('');
    component.toggleResizing();
    expect(component.getResizingColor()).toBe('primary');
    component.toggleResizing();
  });
});
