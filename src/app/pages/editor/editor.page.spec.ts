import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { IonicModule, NavController } from '@ionic/angular';

import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  GraphService,
  graphStubData,
  APP_CONFIG,
  SocketTesting,
  LinkComponent,
  GraphComponent,
  RectTextComponent,
  MultilineTextComponent,
} from 'logic-common';
import { EditorPage } from './editor.page';
import { EditorShapeMenuComponent } from './editor-shape-menu/editor-shape-menu.component';
import { EditorLinkMenuComponent } from './editor-link-menu/editor-link-menu.component';
import { of, throwError } from 'rxjs';
import { SearchPage } from '../search/search.page';
import { GraphTitleDetailsComponent } from 'src/app/components/graph-title-details/graph-title-details.component';
import { Socket } from 'ngx-socket-io';
import { HelperService } from '../../services/helper.service';
import { EditorMainComponent } from './editor-main/editor-main.component';

describe('EditorPage', () => {
  let component: EditorPage;
  let fixture: ComponentFixture<EditorPage>;
  let graphService: GraphService;
  let navCtrl: NavController;
  let helperService: HelperService;
  let route: ActivatedRoute;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EditorPage,
        HeaderComponent,
        SearchPage,
        EditorMainComponent,
        GraphComponent,
        LinkComponent,
        RectTextComponent,
        MultilineTextComponent,
      ],
      imports: [
        IonicModule.forRoot(),
        BrowserDynamicTestingModule,
        RouterTestingModule.withRoutes([
          { path: 'editor/:code', component: EditorPage },
          { path: 'search', component: SearchPage }
        ]),
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteTesting },
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ]
    })
      .overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [
            EditorMainComponent
          ],
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();
    helperService = TestBed.get(HelperService);

    navCtrl = TestBed.get(NavController);

    router = TestBed.get(Router);
    route = TestBed.get(ActivatedRoute);

    fixture = TestBed.createComponent(EditorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init graph on new code param in the route', fakeAsync(() => {
    helperService.showLoading = () => Promise.resolve();
    helperService.hideLoading = () => Promise.resolve();

    spyOn(graphService, 'loadGraph').and.returnValue(of(graphService.data));
    const spyLoadGraph = graphService.loadGraph as jasmine.Spy;

    const spyParams = spyOnProperty(route, 'params', 'get').and.returnValue(of({ code: '2a' }));
    spyOn(component.nav, 'setRoot');
    spyOn(component.nav, 'popToRoot');
    const spyPopToRoot = component.nav.popToRoot as jasmine.Spy;
    spyPopToRoot.and.returnValue(Promise.resolve());
    component.ngOnInit();
    tick();
    expect(spyParams).toHaveBeenCalled();
    expect(spyLoadGraph).not.toHaveBeenCalled();
    expect(spyPopToRoot).toHaveBeenCalled();
    spyPopToRoot.calls.reset();

    spyParams.and.returnValue(of({}));
    component.ngOnInit();
    tick();
    expect(spyLoadGraph).not.toHaveBeenCalled();
    expect(spyPopToRoot).not.toHaveBeenCalled();

    spyParams.and.returnValue(of({ code: '2b' }));
    component.ngOnInit();
    tick();
    expect(spyLoadGraph).toHaveBeenCalled();
    spyLoadGraph.calls.reset();
    expect(spyPopToRoot).toHaveBeenCalled();
    spyPopToRoot.calls.reset();

    spyOn(graphService, 'newGraph').and.returnValue(graphService.newGraph());
    const spyNewGraph = graphService.newGraph as jasmine.Spy;
    spyParams.and.returnValue(of({ code: 'new' }));
    component.ngOnInit();
    tick();
    expect(spyNewGraph).toHaveBeenCalled();
    spyNewGraph.calls.reset();
    expect(spyPopToRoot).toHaveBeenCalled();
    spyPopToRoot.calls.reset();

    spyLoadGraph.and.returnValue(throwError({ statusText: 'DB ERROR' }));
    spyParams.and.returnValue(of({ code: '2b' }));
    component.ngOnInit();
    tick();
    expect(spyLoadGraph).toHaveBeenCalled();
    spyLoadGraph.calls.reset();
    expect(spyPopToRoot).not.toHaveBeenCalled();

    spyLoadGraph.and.returnValue(throwError({ error: { message: 'DB ERROR' } }));
    spyParams.and.returnValue(of({ code: '2b' }));
    component.ngOnInit();
    tick();
    expect(spyLoadGraph).toHaveBeenCalled();
    spyLoadGraph.calls.reset();
    expect(spyPopToRoot).not.toHaveBeenCalled();
  }));

  it('push shape menu', () => {
    spyOn(component.nav, 'push');
    const spy = component.nav.push as jasmine.Spy;
    expect(spy).not.toHaveBeenCalled();
    component.pushShapeMenu(graphService.data.shapes[0]);
    expect(spy).toHaveBeenCalled();
    const arg = spy.calls.mostRecent().args[0];
    expect(arg).toBe(EditorShapeMenuComponent);
  });

  it('push link menu', fakeAsync(() => {
    tick();
    fixture.detectChanges();

    spyOn(component.nav, 'push');
    const spy = component.nav.push as jasmine.Spy;
    expect(spy).not.toHaveBeenCalled();
    component.pushLinkMenu(graphService.data.links[0]);
    expect(spy).toHaveBeenCalled();
    const arg = spy.calls.mostRecent().args[0];
    expect(arg).toBe(EditorLinkMenuComponent);
  }));

  it('push graph title details', () => {
    spyOn(component.nav, 'push');
    const spy = component.nav.push as jasmine.Spy;
    expect(spy).not.toHaveBeenCalled();
    component.pushGraphTitleDetails(graphService.data.title);
    expect(spy).toHaveBeenCalled();
    const arg = spy.calls.mostRecent().args[0];
    expect(arg).toBe(GraphTitleDetailsComponent);
  });

  it('go back button does "pop" via ion-nav of via router', fakeAsync(() => {
    spyOn(navCtrl, 'pop');
    const spyNav = navCtrl.pop as jasmine.Spy;
    spyNav.calls.reset();
    spyOn(component.nav, 'pop');
    const spyIonNav = component.nav.pop as jasmine.Spy;
    spyOn(component.nav, 'canGoBack');
    const spyIonNavCanGoBack = component.nav.canGoBack as jasmine.Spy;
    spyIonNavCanGoBack.and.returnValue(Promise.resolve(true));
    component.goBack();
    tick();
    expect(spyNav).not.toHaveBeenCalled();
    expect(spyIonNav).toHaveBeenCalled();
    spyIonNavCanGoBack.and.returnValue(Promise.resolve(false));
    component.goBack();
    tick();
    expect(spyNav).toHaveBeenCalled();
  }));

  it('should present discard confirm alert', async () => {
    const spy = spyOn(helperService, 'presentDismissAlertConfirm');
    component.presentAlertConfirm();
    expect(spy).toHaveBeenCalled();
  });

});

class ActivatedRouteTesting {
  get params() {
    return of({ code: '2a' });
  }
}
