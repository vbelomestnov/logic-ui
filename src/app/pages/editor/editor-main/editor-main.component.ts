import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GraphData, GraphCalcService, GraphService, GraphComponent } from 'logic-common';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { HelperService } from '../../../services/helper.service';


@Component({
  selector: 'app-editor-main',
  templateUrl: './editor-main.component.html',
  styleUrls: ['./editor-main.component.scss'],
})
export class EditorMainComponent implements OnInit {

  @Input() pushShapeMenu;
  @Input() pushLinkMenu;
  @Input() pushGraphTitleDetails;
  @Input() goBack;

  @ViewChild('graph', { static: false, read: GraphComponent }) graphElement: GraphComponent;

  constructor(
    public graphService: GraphService,
    private helperService: HelperService,
    private router: Router,
    public calcService: GraphCalcService,
  ) { }

  ngOnInit() {
    this.calcService.listenException()
      .subscribe((error: Error) => {
        this.helperService.presentErrorToast(error.message);
      });
  }

  shapeClick({ shape }) {
    this.pushShapeMenu(shape);
  }

  linkClick({ link }) {
    this.pushLinkMenu(link);
  }

  titleDetailsClick() {
    this.pushGraphTitleDetails(this.graphService.data ? this.graphService.data.title : null);
  }

  async save() {
    await this.helperService.showLoading();
    this.graphService.saveGraph(this.graphElement.svgElement.nativeElement.outerHTML)
      .subscribe(
        (result: GraphData) => {
          this.router.navigate(['/preview', result.code]);
          this.helperService.hideLoading();
        },
        (error: HttpErrorResponse) => {
          let message = error.statusText;
          if (error.error && error.error.message) {
            message = error.error.message;
          } else if (error.error && typeof error.error.error === 'string') {
            message = error.error.error;
          }
          this.helperService.presentErrorToast(message);
          this.helperService.hideLoading();
        }
      );
  }
}
