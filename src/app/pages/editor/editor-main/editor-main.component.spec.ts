import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import {
  graphStubData,
  GraphService,
  APP_CONFIG,
  GraphModule,
  Link,
  Shape,
  GraphComponent,
  SocketTesting,
  graphErrorStubData
} from 'logic-common';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { PopoverController, ToastController } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SearchPage } from '../../search/search.page';
import { EditorMainComponent } from './editor-main.component';
import { of, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HelperService } from '../../../services/helper.service';
import { By } from '@angular/platform-browser';
import { Socket } from 'ngx-socket-io';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('EditorMainComponent', () => {
  let component: EditorMainComponent;
  let fixture: ComponentFixture<EditorMainComponent>;
  let graphService: GraphService;
  let router: Router;
  let helperService: HelperService;
  let appGraphComponent: GraphComponent;
  let toastController: ToastController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditorMainComponent, SearchPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes([{ path: 'search', component: SearchPage }]),
        HttpClientTestingModule,
        GraphModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: PopoverController,
          useValue: { create: () => { } },
        },
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [
          ],
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();

    router = TestBed.get(Router);
    helperService = TestBed.get(HelperService);
    toastController = TestBed.get(ToastController);

    fixture = TestBed.createComponent(EditorMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    appGraphComponent = fixture.debugElement.query(By.directive(GraphComponent)).componentInstance as GraphComponent;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('handle shape click and link click', () => {
    graphService.data = graphStubData();
    let shape: Shape = null;
    component.pushShapeMenu = (aShape) => shape = aShape;
    let link: Link = null;
    component.pushLinkMenu = (aLink) => link = aLink;
    expect(appGraphComponent).toBeTruthy();
    appGraphComponent.clickShapeHandler(graphService.data.shapes[0], new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(shape).not.toBeFalsy();
    expect(shape.name).toBe(graphService.data.shapes[0].name);
    appGraphComponent.clickLinkHandler(graphService.data.links[0], new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(link).not.toBeNull();
    expect(link.source + link.dest).toBe(graphService.data.links[0].source + graphService.data.links[0].dest);
  });

  it('handle title details click', () => {
    graphService.data = null;
    let title = '';
    component.pushGraphTitleDetails = (sTitle: string) => title = sTitle;
    component.titleDetailsClick();
    expect(title).toBeNull();
    title = null;
    graphService.data = graphStubData();
    component.titleDetailsClick();
    expect(title).not.toBeNull();
    expect(title).toBe(graphService.data.title);
  });

  it('handle save click', fakeAsync(async () => {
    tick();
    helperService.showLoading = () => Promise.resolve();
    helperService.hideLoading = () => Promise.resolve();
    const spySave = spyOn(graphService, 'saveGraph');
    const data = graphStubData();
    spySave.and.returnValue(of(data));
    const spyNavigate = spyOn(router, 'navigate');
    await component.save();
    tick();
    expect(spyNavigate).toHaveBeenCalled();
    const arg = spyNavigate.calls.mostRecent().args[0];
    expect(arg instanceof Array).toBe(true);
    expect(arg[0]).toBe('/preview');
    expect(arg[1]).toBe(data.code);
    spyNavigate.calls.reset();

    const errorMessageStatusText = 'DB ERROR status text';
    const spyToast = spyOn(toastController, 'create').and.returnValue(Promise.resolve({ present: () => { } } as HTMLIonToastElement));
    spySave.and.returnValue(throwError({ statusText: errorMessageStatusText }));
    await component.save();
    tick();
    expect(spyNavigate).not.toHaveBeenCalled();
    expect(spyToast).toHaveBeenCalled();
    let toastArg = spyToast.calls.mostRecent().args[0];
    expect(toastArg.message).toBe(errorMessageStatusText);
    spyToast.calls.reset();

    const errorMessageMessage = graphErrorStubData();
    spySave.and.returnValue(throwError({ error: { message: errorMessageMessage } }));
    await component.save();
    tick();
    expect(spyNavigate).not.toHaveBeenCalled();
    expect(spyToast).toHaveBeenCalled();
    toastArg = spyToast.calls.mostRecent().args[0];
    expect(toastArg.message).toBe(
      `title :: title must be longer than or equal to 10 characters
shapes :: 0 :: text :: text must be longer than or equal to 10 characters`
    );
    spyToast.calls.reset();

    const errorMessageError = 'DB ERROR';
    spySave.and.returnValue(throwError({ error: { error: errorMessageError } }));
    await component.save();
    tick();
    expect(spyNavigate).not.toHaveBeenCalled();
    expect(spyToast).toHaveBeenCalled();
    toastArg = spyToast.calls.mostRecent().args[0];
    expect(toastArg.message).toBe(errorMessageError);
  }));
});
