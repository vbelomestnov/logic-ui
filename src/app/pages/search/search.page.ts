import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { RestApiService, GraphShortData } from 'logic-common';
import { NavController, Platform, IonInfiniteScroll, IonSearchbar } from '@ionic/angular';
import { Router } from '@angular/router';
import { CanBackPage } from '../../pages/can-back.page';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage extends CanBackPage implements OnInit {

  private offset = 0;
  public graphList: GraphShortData[] = [];
  private searchText = '';

  @ViewChild(IonInfiniteScroll, { static: true }) ionInfinite: IonInfiniteScroll;
  @ViewChild(IonSearchbar, { static: false }) searchInput: IonSearchbar;

  constructor(
    private restApiService: RestApiService,
    protected router: Router,
    protected helperService: HelperService,
    protected navCtrl: NavController,
    protected platform: Platform,
    private ngZone: NgZone,
  ) {
    super(navCtrl, platform, router, helperService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.activationEndEmitter.subscribe(async () => {
      this.graphList = [];
      this.offset = 0;
      await this.loadData();
    });
  }

  async loadData() {
    await this.helperService.showLoading();
    try {
      const result: GraphShortData[] = await this.restApiService.getGraphList(this.searchText, this.offset).toPromise();
      if (result.length) {
        this.offset += result.length;
        this.graphList = this.graphList.concat(result);
      }
      this.ionInfinite.complete();
      await this.helperService.hideLoading();
    } catch(error) {
      let message = error.statusText;
      if (error.error && error.error.message) {
        message = error.error.message;
      }
      this.helperService.presentErrorToast(message);
      this.ionInfinite.complete();
      await this.helperService.hideLoading();
    }
  }

  async searchChange(event: CustomEvent) {
    this.searchText = event.detail.value;
    this.graphList = [];
    this.offset = 0;
    await this.loadData();
    this.searchInput.setFocus();
  }

  async handleRefresher(event) {
    this.graphList = [];
    this.offset = 0;
    await this.loadData();
    event.target.complete();
  }

  newGraph(): void {
    this.router.navigate(['/editor', 'new']);
  }

  goBack(): void {
    this.ngZone.run(() => {
      this.helperService.canExit = true;
    });
  }

}
