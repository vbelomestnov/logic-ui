import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { SearchPage } from './search.page';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { graphStubData, GraphService, APP_CONFIG, SocketTesting } from 'logic-common';
import { EditorPage } from '../editor/editor.page';
import { throwError, of } from 'rxjs';
import { Router } from '@angular/router';
import { RestApiService } from 'logic-common';
import { IonicModule } from '@ionic/angular';
import { HelperService } from '../../services/helper.service';
import { Socket } from 'ngx-socket-io';

describe('SearchPage', () => {
  let component: SearchPage;
  let fixture: ComponentFixture<SearchPage>;
  let router: Router;
  let restApiService: RestApiService;
  let graphService: GraphService;
  let helperService: HelperService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchPage, EditorPage],
      imports: [
        IonicModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule,
        RouterTestingModule.withRoutes([
          { path: 'editor/:code', component: EditorPage },
          { path: 'search', component: SearchPage }
        ]),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    restApiService = TestBed.get(RestApiService);
    graphService = TestBed.get(GraphService);
    router = TestBed.get(Router);
    helperService = TestBed.get(HelperService);

    fixture = TestBed.createComponent(SearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init graph on route activation end', fakeAsync(() => {
    helperService.showLoading = () => Promise.resolve();
    helperService.hideLoading = () => Promise.resolve();
    component.ionInfinite.complete = () => Promise.resolve();

    const data = graphStubData();
    spyOn(restApiService, 'getGraphList');
    const spyGetGraphList = restApiService.getGraphList as jasmine.Spy;

    spyGetGraphList.and.returnValue(of([{ code: data.code, title: data.title }]));
    router.navigate(['/search']);
    tick();
    expect(spyGetGraphList).toHaveBeenCalled();
    spyGetGraphList.calls.reset();
    expect(component.graphList.length).toBe(1);

    spyGetGraphList.and.returnValue(of([]));
    router.navigate(['/editor', 'a']);
    tick();
    router.navigate(['/search']);
    tick();
    expect(spyGetGraphList).toHaveBeenCalled();
    spyGetGraphList.calls.reset();
    expect(component.graphList.length).toBe(0);

    spyGetGraphList.and.returnValue(throwError({ statusText: 'DB ERROR' }));
    router.navigate(['/editor', 'b']);
    tick();
    router.navigate(['/search']);
    tick();
    expect(spyGetGraphList).toHaveBeenCalled();
    spyGetGraphList.calls.reset();
    expect(component.graphList.length).toBe(0);

    spyGetGraphList.and.returnValue(throwError({ error: { message: 'DB ERROR' } }));
    router.navigate(['/editor', 'c']);
    tick();
    router.navigate(['/search']);
    tick();
    expect(spyGetGraphList).toHaveBeenCalled();
    spyGetGraphList.calls.reset();
    expect(component.graphList.length).toBe(0);
  }));

  it('should handle search change event', async () => {
    spyOn(restApiService, 'getGraphList');
    const spyGetGraphList = restApiService.getGraphList as jasmine.Spy;
    spyGetGraphList.and.returnValue(of([{ code: 'a', title: 'b' }]));

    const searchValue = 'asdf';
    const event = new CustomEvent('search', { detail: { value: searchValue } });
    component.graphList = [{ code: 'a', title: 'b' }];
    await component.searchChange(event);
    expect(component.graphList.length).toBe(1);
    expect(spyGetGraphList).toHaveBeenCalled();
    const args = spyGetGraphList.calls.mostRecent().args;
    expect(args[0]).toBe(searchValue);
    expect(args[1]).toBe(0);
  });

  it('loadData concatenates data', async () => {
    spyOn(restApiService, 'getGraphList');
    const spyGetGraphList = restApiService.getGraphList as jasmine.Spy;
    spyGetGraphList.and.returnValue(of([{ code: 'a', title: 'b' }]));

    await component.loadData();
    expect(component.graphList.length).toBe(1);
    expect(spyGetGraphList).toHaveBeenCalled();
    let args = spyGetGraphList.calls.mostRecent().args;
    expect(args[0]).toBe('');
    expect(args[1]).toBe(0);

    await component.loadData();
    expect(component.graphList.length).toBe(2);
    expect(spyGetGraphList).toHaveBeenCalled();
    args = spyGetGraphList.calls.mostRecent().args;
    expect(args[0]).toBe('');
    expect(args[1]).toBe(1);
  });

  it('handle New Graph click', fakeAsync(() => {
    spyOn(router, 'navigate');
    const spyNavigate = router.navigate as jasmine.Spy;
    component.newGraph();
    const args = spyNavigate.calls.mostRecent().args[0];
    expect(args[0]).toBe('/editor');
    expect(args[1]).toBe('new');
  }));

  it('Go Back activates exit sequence', fakeAsync(() => {
    expect(helperService.canExit).toBe(false);
    component.goBack();
    expect(helperService.canExit).toBe(true);
  }));

  it('handle refresher', async () => {
    const stubEvent = {
      target: {
        complete: () => { }
      }
    };
    component.loadData = async () => { };

    const spy = spyOn(stubEvent.target, 'complete');
    await component.handleRefresher(stubEvent);
    expect(spy).toHaveBeenCalled();
  });
});
