import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { ShapeNewPositionPage } from './shape-new-position.page';
import { RouterTestingModule } from '@angular/router/testing';
import { GraphService, graphStubData, APP_CONFIG, SocketTesting } from 'logic-common';
import { Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { of } from 'rxjs';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EditorPage } from '../editor/editor.page';
import { Socket } from 'ngx-socket-io';
import { HelperService } from '../../services/helper.service';

const platform = {
  backButton: of({})
};

describe('ShapeNewPositionPage', () => {
  let component: ShapeNewPositionPage;
  let fixture: ComponentFixture<ShapeNewPositionPage>;
  let router: Router;
  let graphService: GraphService;
  let helperService: HelperService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShapeNewPositionPage, HeaderComponent, EditorPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'can', children: [{ path: 'back', component: ShapeNewPositionPage }] },
          { path: 'editor/:code', component: EditorPage }
        ]),
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: NavController,
          useValue: {},
        },
        {
          provide: Platform,
          useValue: platform,
        },
        { provide: APP_CONFIG, useValue: {}},
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();
    helperService = TestBed.get(HelperService);
    fixture = TestBed.createComponent(ShapeNewPositionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect on "/editor" if data.newShapes is empty', () => {
    spyOn(router, 'navigate');
    const spy = router.navigate as jasmine.Spy;
    spy.calls.reset();
    component.ngOnInit();
    expect(spy.calls.mostRecent()).not.toBeUndefined();
    const navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs[0]).toBe('/editor');
    spy.calls.reset();
    graphService.queueNewShape('New Shape');
    component.ngOnInit();
    expect(spy.calls.mostRecent()).toBeUndefined();
    graphService.refuseNewShapes();
  });

  it('shape created handler', () => {
    spyOn(router, 'navigate');
    const spy = router.navigate as jasmine.Spy;
    spy.calls.reset();
    component.shapeCreated(graphService.data.shapes[0]);
    expect(spy.calls.mostRecent()).not.toBeUndefined();
    let navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs[0]).toBe('/editor');

    graphService.newGraph();
    component.shapeCreated(graphService.data.shapes[0]);
    expect(spy).toHaveBeenCalledTimes(2);
    navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs).toEqual(['/editor', 'new']);
  });

  it('go back button handler refuses data.newShapes', () => {
    graphService.queueNewShape('text');
    expect(graphService.newShapes.length).toBe(1);
    spyOn(router, 'navigate');
    const spy = router.navigate as jasmine.Spy;
    spy.calls.reset();
    component.goBack();
    let navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs[0]).toBe('/editor');
    expect(graphService.newShapes.length).toBe(0);

    graphService.newGraph();
    component.goBack();
    expect(spy).toHaveBeenCalledTimes(2);
    navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs).toEqual(['/editor', 'new']);
  });

  it('should navigate back when user click native "back" button', fakeAsync(() => {
    spyOn(component, 'goBack');
    const spy = component.goBack as jasmine.Spy;
    graphService.queueNewShape('Test shape');
    router.navigate(['/can/back']);
    component.ngOnInit();
    tick();
    expect(spy).toHaveBeenCalled();
    component.ngOnDestroy();
    graphService.refuseNewShapes();
  }));

  it('should present discard confirm alert', async () => {
    const spy = spyOn(helperService, 'presentDismissAlertConfirm');
    component.presentAlertConfirm();
    expect(spy).toHaveBeenCalled();
  });
});
