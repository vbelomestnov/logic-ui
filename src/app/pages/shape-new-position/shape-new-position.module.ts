import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ShapeNewPositionPage } from './shape-new-position.page';
import { GraphModule } from 'logic-common';
import { HeaderModule } from 'src/app/components/header/header.module';
import { DiscardGuard } from 'src/app/discard.guard';

const routes: Routes = [
  {
    path: '',
    component: ShapeNewPositionPage,
    canDeactivate: [DiscardGuard],
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    GraphModule,
    HeaderModule,
  ],
  declarations: [ShapeNewPositionPage]
})
export class ShapeNewPositionPageModule {}
