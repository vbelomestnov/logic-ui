import { Component, OnInit } from '@angular/core';
import { GraphService, GraphCalcService, GraphMode, Shape, } from 'logic-common';
import { Router } from '@angular/router';
import { CanBackPage } from '../can-back.page';
import { Platform, NavController } from '@ionic/angular';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-shape-new-position',
  templateUrl: './shape-new-position.page.html',
  styleUrls: ['./shape-new-position.page.scss'],
})
export class ShapeNewPositionPage extends CanBackPage implements OnInit {

  GraphMode = GraphMode;

  constructor(
    public graphService: GraphService,
    protected router: Router,
    protected navCtrl: NavController,
    protected platform: Platform,
    protected helperService: HelperService,
    public calcService: GraphCalcService,
  ) {
    super(navCtrl, platform, router, helperService);
  }

  ngOnInit() {
    if (!this.graphService.newShapes.length) {
      this.router.navigate(['/editor', 'new']);
    }

    super.ngOnInit();
  }

  shapeCreated(newShape: Shape) {
    this.router.navigate(['/editor', this.graphService.data.code || 'new']);
  }

  goBack() {
    this.graphService.refuseNewShapes();
    this.router.navigate(['/editor', this.graphService.data.code || 'new']);
  }

  presentAlertConfirm() {
    return this.helperService.presentDismissAlertConfirm();
  }
}
