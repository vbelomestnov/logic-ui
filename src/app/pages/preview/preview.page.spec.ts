import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { PreviewPage } from './preview.page';
import { RouterTestingModule } from '@angular/router/testing';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SearchPage } from '../search/search.page';
import { Router, ActivatedRoute } from '@angular/router';
import { GraphService, graphStubData, APP_CONFIG, SocketTesting } from 'logic-common';
import { of, throwError } from 'rxjs';
import { IonicModule, NavController } from '@ionic/angular';
import { GraphTitleDetailsComponent } from 'src/app/components/graph-title-details/graph-title-details.component';
import { Socket } from 'ngx-socket-io';
import { HelperService } from 'src/app/services/helper.service';

describe('PreviewPage', () => {
  let component: PreviewPage;
  let fixture: ComponentFixture<PreviewPage>;
  let router: Router;
  let graphService: GraphService;
  let route: ActivatedRoute;
  let navCtrl: NavController;
  let helperService: HelperService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PreviewPage,
        HeaderComponent,
        SearchPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        IonicModule.forRoot(),
        RouterTestingModule.withRoutes([
          { path: 'preview/:code', component: PreviewPage },
          { path: 'search', component: SearchPage }
        ]),
        HttpClientTestingModule
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteTesting },
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();

    navCtrl = TestBed.get(NavController);

    router = TestBed.get(Router);
    route = TestBed.get(ActivatedRoute);
    helperService = TestBed.get(HelperService);

    fixture = TestBed.createComponent(PreviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init graph on route activation end', fakeAsync(() => {
    helperService.showLoading = () => Promise.resolve();
    helperService.hideLoading = () => Promise.resolve();

    spyOn(graphService, 'loadGraph').and.returnValue(of(graphService.data));
    const spyLoadGraph = graphService.loadGraph as jasmine.Spy;

    const spyParams = spyOnProperty(route, 'params', 'get').and.returnValue(of({ code: '2a' }));

    spyOn(component.nav, 'setRoot');
    spyOn(component.nav, 'popToRoot');
    const spyPopToRoot = component.nav.popToRoot as jasmine.Spy;
    spyPopToRoot.and.returnValue(Promise.resolve());

    component.ngOnInit();
    tick();
    expect(spyParams).toHaveBeenCalled();
    expect(spyLoadGraph).toHaveBeenCalled();
    expect(spyPopToRoot).toHaveBeenCalled();
    spyLoadGraph.calls.reset();
    spyPopToRoot.calls.reset();

    spyParams.and.returnValue(of({}));
    component.ngOnInit();
    tick();
    expect(spyLoadGraph).not.toHaveBeenCalled();
    expect(spyPopToRoot).not.toHaveBeenCalled();

    spyLoadGraph.and.returnValue(throwError({ statusText: 'DB ERROR' }));
    spyParams.and.returnValue(of({ code: '2b' }));
    component.ngOnInit();
    tick();
    expect(spyLoadGraph).toHaveBeenCalled();
    spyLoadGraph.calls.reset();
    expect(spyPopToRoot).not.toHaveBeenCalled();

    spyLoadGraph.and.returnValue(throwError({ error: { message: 'DB ERROR' } }));
    spyParams.and.returnValue(of({ code: '2b' }));
    component.ngOnInit();

    tick();
    expect(spyLoadGraph).toHaveBeenCalled();
    spyLoadGraph.calls.reset();
    expect(spyPopToRoot).not.toHaveBeenCalled();
  }));

  it('push graph title details', () => {
    spyOn(component.nav, 'push');
    const spy = component.nav.push as jasmine.Spy;
    expect(spy).not.toHaveBeenCalled();
    component.pushGraphTitleDetails(graphService.data.title);
    expect(spy).toHaveBeenCalled();
    const arg = spy.calls.mostRecent().args[0];
    expect(arg).toBe(GraphTitleDetailsComponent);
  });

  it('go back button does "pop" via ion-nav of via router', fakeAsync(() => {
    spyOn(navCtrl, 'pop');
    const spyNav = navCtrl.pop as jasmine.Spy;
    spyNav.calls.reset();
    spyOn(component.nav, 'pop');
    const spyIonNav = component.nav.pop as jasmine.Spy;
    spyOn(component.nav, 'canGoBack');
    const spyIonNavCanGoBack = component.nav.canGoBack as jasmine.Spy;
    spyIonNavCanGoBack.and.returnValue(Promise.resolve(true));
    component.goBack();
    tick();
    expect(spyNav).not.toHaveBeenCalled();
    expect(spyIonNav).toHaveBeenCalled();
    spyIonNavCanGoBack.and.returnValue(Promise.resolve(false));
    component.goBack();
    tick();
    expect(spyNav).toHaveBeenCalled();
  }));
});

class ActivatedRouteTesting {
  get params() {
    return of({ code: '2a' });
  }
}
