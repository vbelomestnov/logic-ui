import { Component, OnInit, Input, Inject } from '@angular/core';
import { GraphData, GraphMode, APP_CONFIG, AppConfig } from 'logic-common';
import { HelperService } from 'src/app/services/helper.service';
import { VK_OPEN_API, VkOpenApi } from 'src/app/types/vk-open-api';

@Component({
  selector: 'app-preview-main',
  templateUrl: './preview-main.component.html',
  styleUrls: ['./preview-main.component.scss'],
})
export class PreviewMainComponent implements OnInit {
  GraphMode = GraphMode;

  @Input() data: GraphData;
  @Input() pushGraphTitleDetails;
  @Input() goBack;
  @Input() loadGraph;

  constructor(
    private helperService: HelperService,
    @Inject(APP_CONFIG) private appConfig: AppConfig,
    @Inject(VK_OPEN_API) private vkOpenApi: VkOpenApi
  ) { }

  ngOnInit() { }

  titleDetailsClick() {
    this.pushGraphTitleDetails(this.data ? this.data.title : null);
  }

  async postVkClick() {
    await this.helperService.showLoading();
    try {
      this.helperService.publicationInProgress = true;
      await this.vkOpenApi.login();
      await this.vkOpenApi.sharePost({
        message: `Code: ${this.data.code} "${this.data.title}"`,
        link: `${this.appConfig.STATIC_URL}/graph/${this.data.code}`
      });
      this.helperService.presentSuccessToast();
    } catch (error) {
      this.helperService.presentErrorToast(`VK error ${error.message}`);
    } finally {
      this.helperService.hideLoading();
      this.helperService.publicationInProgress = false;
    }
  }

  async handleRefresher(event) {
    await this.loadGraph();
    event.target.complete();
  }
}
