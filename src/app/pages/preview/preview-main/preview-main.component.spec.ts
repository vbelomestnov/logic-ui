import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { PreviewMainComponent } from './preview-main.component';
import { graphStubData, APP_CONFIG, SocketTesting } from 'logic-common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Socket } from 'ngx-socket-io';
import { HelperService } from 'src/app/services/helper.service';
import { By } from '@angular/platform-browser';
import { ToastController } from '@ionic/angular';
import { VK_OPEN_API, VkOpenApi } from 'src/app/types/vk-open-api';

describe('PreviewMainComponent', () => {
  let component: PreviewMainComponent;
  let fixture: ComponentFixture<PreviewMainComponent>;
  let helperService: HelperService;
  let toastController: ToastController;
  let vkOpenApi: VkOpenApi;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PreviewMainComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
        { provide: VK_OPEN_API, useValue: VkOpenApi},
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    helperService = TestBed.get(HelperService);
    toastController = TestBed.get(ToastController);
    vkOpenApi = TestBed.get(VK_OPEN_API);

    fixture = TestBed.createComponent(PreviewMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('handle title details click', () => {
    component.data = null;
    let title = '';
    component.pushGraphTitleDetails = (sTitle: string) => title = sTitle;
    component.titleDetailsClick();
    expect(title).toBeNull();
    title = null;
    component.data = graphStubData();
    component.titleDetailsClick();
    expect(title).not.toBeNull();
    expect(title).toBe(component.data.title);
  });

  it('handle refresher', async () => {
    const stubEvent = {
      target: {
        complete: () => { }
      }
    };
    component.loadGraph = () => { };

    const spy = spyOn(stubEvent.target, 'complete');
    await component.handleRefresher(stubEvent);
    expect(spy).toHaveBeenCalled();
  });

  it('handle vk publishing', fakeAsync(() => {
    component.data = graphStubData();
    const spyToast = spyOn(toastController, 'create').and.returnValue(Promise.resolve({ present: () => { } } as HTMLIonToastElement));
    const spyErrorToast = spyOn(helperService, 'presentErrorToast').and.returnValue(Promise.resolve());
    helperService.showLoading = () => Promise.resolve();
    helperService.hideLoading = () => Promise.resolve();
    const ionButtons = fixture.debugElement.queryAll(By.css('ion-button'));
    ionButtons[0].nativeElement.click();
    tick();
    expect(spyToast).toHaveBeenCalled();
    expect(spyErrorToast).not.toHaveBeenCalled();

    spyOn(vkOpenApi, 'sharePost').and.returnValue(Promise.reject('Test error'));
    ionButtons[0].nativeElement.click();
    tick();
    expect(spyToast).toHaveBeenCalledTimes(1);
    expect(spyErrorToast).toHaveBeenCalled();
  }));
});


// tslint:disable-next-line:variable-name
const VkOpenApi = {
  login: () => Promise.resolve(),
  sharePost: () => Promise.resolve(),
};