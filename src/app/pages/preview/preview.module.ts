import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PreviewPage } from './preview.page';
import { GraphModule } from 'logic-common';
import { HeaderModule } from 'src/app/components/header/header.module';
import { PreviewMainComponent } from './preview-main/preview-main.component';
import { GraphTitleDetailsModule } from 'src/app/components/graph-title-details/graph-title-details.module';
import { VK_OPEN_API } from 'src/app/types/vk-open-api';
import { Plugins } from '@capacitor/core';

const routes: Routes = [
  {
    path: '',
    component: PreviewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    GraphModule,
    HeaderModule,
    GraphTitleDetailsModule,
  ],
  declarations: [PreviewPage, PreviewMainComponent],
  entryComponents: [PreviewMainComponent],
  providers: [
    { provide: VK_OPEN_API, useValue: Plugins.VkOpenApi},
  ]
})
export class PreviewPageModule { }
