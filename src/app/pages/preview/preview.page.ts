import { Component, OnInit, ViewChild } from '@angular/core';
import { GraphService, GraphCalcService } from 'logic-common';
import { GraphData } from 'logic-common';
import { CanBackPage } from '../can-back.page';
import { Platform, NavController, IonNav } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { PreviewMainComponent } from './preview-main/preview-main.component';
import { GraphTitleDetailsComponent } from 'src/app/components/graph-title-details/graph-title-details.component';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.page.html',
  styleUrls: ['./preview.page.scss'],
})
export class PreviewPage extends CanBackPage implements OnInit {

  root = PreviewMainComponent;
  rootParams = null;
  code: string = null;

  @ViewChild(IonNav, { static: false }) nav: IonNav;

  constructor(
    private graphService: GraphService,
    protected navCtrl: NavController,
    protected platform: Platform,
    protected router: Router,
    protected helperService: HelperService,
    private route: ActivatedRoute,
    private calcService: GraphCalcService,
  ) {
    super(navCtrl, platform, router, helperService);
    this.pushGraphTitleDetails = this.pushGraphTitleDetails.bind(this);
    this.goBack = this.goBack.bind(this);
    this.loadGraph = this.loadGraph.bind(this);
  }

  ngOnInit() {
    this.rootParams = {
      data: this.graphService.data,
      pushGraphTitleDetails: this.pushGraphTitleDetails,
      goBack: this.goBack,
      loadGraph: this.loadGraph,
    };
    super.ngOnInit();
    this.route.params.subscribe(
      async params => {
        const code = params.code;
        if (code && code.length) {
          this.code = code;
          await this.loadGraph();
        } else {
          this.router.navigate(['/search']);
        }
      }
    );
  }

  async loadGraph() {
    await this.helperService.showLoading();
    this.graphService.loadGraph(this.code)
      .subscribe(
        (result: GraphData) => {
          this.pushMain(result);
          this.helperService.hideLoading();
        },
        (error: HttpErrorResponse) => {
          let message = error.statusText;
          if (error.error && error.error.message) {
            message = error.error.message;
          }
          this.helperService.presentErrorToast(message);
          this.helperService.hideLoading();
        }
      );
  }

  pushMain(data: GraphData) {
    this.rootParams = {
      ...this.rootParams,
      data
    };
    this.nav.setRoot(PreviewMainComponent, this.rootParams);
    this.nav.popToRoot();
  }

  pushGraphTitleDetails(title) {
    return this.nav.push(GraphTitleDetailsComponent, {
      title,
      readonly: true,
      goBack: this.goBack,
    });
  }

  goBack() {
    this.nav.canGoBack()
      .then(
        can => {
          if (can) {
            this.nav.pop();
          } else {
            this.navCtrl.pop();
          }
        }
      );
  }

}
