import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShapeEditPage } from './shape-edit.page';
import { RouterTestingModule } from '@angular/router/testing';
import { GraphService, graphStubData, APP_CONFIG, SocketTesting } from 'logic-common';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { NavController } from '@ionic/angular';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Socket } from 'ngx-socket-io';
import { HelperService } from '../../services/helper.service';

describe('ShapeEditPage', () => {
  let component: ShapeEditPage;
  let fixture: ComponentFixture<ShapeEditPage>;
  let router: Router;
  let route: ActivatedRoute;
  let graphService: GraphService;
  let helperService: HelperService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShapeEditPage, HeaderComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            params: of({})
          }
        },
        {
          provide: NavController, useValue: {}
        },
        { provide: APP_CONFIG, useValue: {}},
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();
    helperService = TestBed.get(HelperService);
    fixture = TestBed.createComponent(ShapeEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    route = TestBed.get(ActivatedRoute);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get shape recieved via route', () => {
    expect(component.shape).toBeFalsy();
    route.params = of({ name: graphService.data.shapes[0].name });
    component.ngOnInit();
    expect(component.shape).not.toBeNull();
    expect(component.value).toBe(graphService.data.shapes[0].text);
  });

  it('submit handler', () => {
    spyOn(router, 'navigate');
    const spy = router.navigate as jasmine.Spy;
    const spySaveShapeText = spyOn(graphService, 'saveShapeText');
    const newValue = 'New Value';
    component.value = newValue;
    component.submit();
    let navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs[0]).toBe('/shape/new/position');
    spy.calls.reset();
    route.params = of({ name: graphService.data.shapes[0].name });
    component.ngOnInit();
    component.value = newValue;
    component.submit();
    expect(spySaveShapeText).toHaveBeenCalled();

    graphService.data.code = '';
    component.value = newValue;
    component.submit();
    expect(spy).toHaveBeenCalledTimes(2);
    navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs).toEqual(['/editor', 'new']);
  });

  it('should present discard confirm alert', async () => {
    const spy = spyOn(helperService, 'presentDismissAlertConfirm');
    component.presentAlertConfirm();
    expect(spy).toHaveBeenCalled();
  });
});
