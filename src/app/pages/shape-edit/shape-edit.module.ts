import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ShapeEditPage } from './shape-edit.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { DiscardGuard } from 'src/app/discard.guard';

const routes: Routes = [
  {
    path: '',
    component: ShapeEditPage,
    canDeactivate: [DiscardGuard],
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    HeaderModule,
  ],
  declarations: [ShapeEditPage]
})
export class ShapeEditPageModule { }
