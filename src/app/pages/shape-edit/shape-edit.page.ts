import { Component, OnInit } from '@angular/core';
import { GraphService } from 'logic-common';
import { Router, ActivatedRoute } from '@angular/router';
import { Shape } from 'logic-common';
import { NavController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { CanBackPage } from '../can-back.page';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-shape-edit',
  templateUrl: './shape-edit.page.html',
  styleUrls: ['./shape-edit.page.scss'],
})
export class ShapeEditPage extends CanBackPage implements OnInit {

  public value = '';
  public shape: Shape = null;

  constructor(
    public graphService: GraphService,
    protected helperService: HelperService,
    protected router: Router,
    private route: ActivatedRoute,
    protected navCtrl: NavController,
    protected platform: Platform,
  ) {
    super(navCtrl, platform, router, helperService);
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        const shapeName = params.name;
        this.shape = this.graphService.getShape(shapeName);
        if (this.shape) {
          this.value = this.shape.text;
        }
      }
    );
    super.ngOnInit();
  }

  submit() {
    if (!this.shape) {
      this.graphService.queueNewShape(this.value);
      this.router.navigate(['/shape/new/position']);
    } else {
      this.graphService.saveShapeText(this.shape.name, this.value);
      this.router.navigate(['/editor', this.graphService.data.code || 'new']);
    }
  }

  presentAlertConfirm() {
    return this.helperService.presentDismissAlertConfirm();
  }

}
