import { Component, OnInit } from '@angular/core';
import { GraphService, GraphCalcService, GraphMode } from 'logic-common';
import { Router } from '@angular/router';
import { CanBackPage } from '../can-back.page';
import { NavController, Platform } from '@ionic/angular';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-link-new',
  templateUrl: './link-new.page.html',
  styleUrls: ['./link-new.page.scss'],
})
export class LinkNewPage extends CanBackPage implements OnInit {

  GraphMode = GraphMode;

  constructor(
    public graphService: GraphService,
    protected router: Router,
    protected navCtrl: NavController,
    protected platrofm: Platform,
    protected helperService: HelperService,
    public calcService: GraphCalcService,
  ) {
    super(navCtrl, platrofm, router, helperService);
  }

  ngOnInit() {
    if (!this.graphService.data || !this.graphService.newLinks.length) {
      this.graphService.refuseNewLinks();
      this.router.navigate(['/editor', this.graphService.data.code || 'new']);
    }
    super.ngOnInit();
  }

  shapeClick({ shape }) {
    this.graphService.newLink(shape.name)
      .then(() => {
        this.router.navigate(['/editor', this.graphService.data.code || 'new']);
      })
      .catch(error => {
        this.helperService.presentErrorToast(error.message);
      });
  }

  goBack() {
    this.graphService.refuseNewLinks();
    this.router.navigate(['/editor', this.graphService.data.code || 'new']);
  }

  presentAlertConfirm() {
    return this.helperService.presentDismissAlertConfirm();
  }

}
