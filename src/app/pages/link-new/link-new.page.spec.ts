import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { LinkNewPage } from './link-new.page';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { GraphService, graphStubData, APP_CONFIG, SocketTesting } from 'logic-common';
import { NavController, Platform } from '@ionic/angular';
import { of } from 'rxjs';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HelperService } from '../../services/helper.service';
import { EditorPage } from '../editor/editor.page';
import { Socket } from 'ngx-socket-io';

const platform = {
  backButton: of({})
};

describe('LinkNewPage', () => {
  let component: LinkNewPage;
  let fixture: ComponentFixture<LinkNewPage>;
  let router: Router;
  let graphService: GraphService;
  let helperService: HelperService;
  let socket: SocketTesting;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinkNewPage, HeaderComponent, EditorPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'can', children: [{ path: 'back', component: LinkNewPage }] },
          { path: 'editor/:code', component: EditorPage }
        ]),
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: NavController,
          useValue: {},
        },
        {
          provide: Platform,
          useValue: platform,
        },
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    graphService = TestBed.get(GraphService);
    graphService.data = graphStubData();
    socket = TestBed.get(Socket);
    fixture = TestBed.createComponent(LinkNewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    helperService = TestBed.get(HelperService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect on "/editor" if data.newLinks is empty', () => {
    const data = graphService.data;
    const shape = data.shapes[0];
    spyOn(router, 'navigate');
    const spy = router.navigate as jasmine.Spy;
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
    let navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs[0]).toBe('/editor');
    graphService.queueNewLink(shape.name, 'dest');
    component.ngOnInit();
    expect(spy).toHaveBeenCalledTimes(1);
    graphService.refuseNewLinks();
    graphService.newGraph();
    component.ngOnInit();
    expect(spy).toHaveBeenCalledTimes(2);
    navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs).toEqual(['/editor', 'new']);
  });

  it('select second shape for link creation', fakeAsync(() => {
    const shape = graphService.data.shapes[0];
    graphService.queueNewLink(shape.name, 'dest');
    expect(graphService.newLinks.length).toBe(1);

    spyOn(router, 'navigate');
    const spy = router.navigate as jasmine.Spy;
    component.shapeClick({ shape: graphService.data.shapes[1] });
    tick();
    expect(spy).toHaveBeenCalled();

    graphService.data.code = '';
    component.shapeClick({ shape: graphService.data.shapes[1] });
    tick();
    expect(spy).toHaveBeenCalledTimes(2);
    const navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).toEqual(['/editor', 'new']);

    socket._graphSuccess = false;
    socket._exceptionMessage = 'test message';
    spyOn(helperService, 'presentErrorToast');
    const spyPresentToast = helperService.presentErrorToast as jasmine.Spy;
    spy.calls.reset();
    component.shapeClick({ shape });
    tick();
    expect(spyPresentToast).toHaveBeenCalled();
    expect(spy).not.toHaveBeenCalled();
  }));

  it('go back button handler refuses data.newLinks', fakeAsync(() => {
    const shape = graphService.data.shapes[0];
    graphService.queueNewLink(shape.name, 'dest');
    expect(graphService.newLinks.length).toBe(1);
    spyOn(router, 'navigate');
    const spy = router.navigate as jasmine.Spy;
    component.goBack();
    let navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).not.toBeNull();
    expect(navArgs[0]).toBe('/editor');
    expect(graphService.newLinks.length).toBe(0);

    graphService.data.code = '';
    component.goBack();
    tick();
    expect(spy).toHaveBeenCalledTimes(2);
    navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs).toEqual(['/editor', 'new']);
  }));

  it('should navigate back when user click native "back" button', fakeAsync(() => {
    const shape = graphService.data.shapes[0];
    spyOn(component, 'goBack');
    const spy = component.goBack as jasmine.Spy;
    graphService.queueNewLink(shape.name, 'dest');
    router.navigate(['/can/back']);
    component.ngOnInit();
    tick();
    expect(spy).toHaveBeenCalled();
    component.ngOnDestroy();
    graphService.refuseNewLinks();
  }));

  it('should present discard confirm alert', async () => {
    const spy = spyOn(helperService, 'presentDismissAlertConfirm');
    component.presentAlertConfirm();
    expect(spy).toHaveBeenCalled();
  });
});
