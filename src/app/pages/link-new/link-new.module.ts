import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LinkNewPage } from './link-new.page';
import { GraphModule } from 'logic-common';
import { HeaderModule } from 'src/app/components/header/header.module';
import { DiscardGuard } from 'src/app/discard.guard';

const routes: Routes = [
  {
    path: '',
    component: LinkNewPage,
    canDeactivate: [DiscardGuard],
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    GraphModule,
    HeaderModule,
  ],
  declarations: [LinkNewPage]
})
export class LinkNewPageModule {}
