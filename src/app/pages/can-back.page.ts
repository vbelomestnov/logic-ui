import { NavController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { Router, ActivationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subscription, Subject } from 'rxjs';
import { OnInit, OnDestroy, Component } from '@angular/core';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-can-back',
  template: '',
})
export class CanBackPage implements OnInit, OnDestroy {

  protected routerSubscription: Subscription = null;
  protected activationEndEmitter: Subject<null> = new Subject<null>();

  constructor(
    protected navCtrl: NavController,
    protected platform: Platform,
    protected router: Router,
    protected helperService: HelperService,
  ) { }

  ngOnInit() {
    this.routerSubscription = this.router.events.pipe(
      filter(event => event instanceof ActivationEnd)
    )
      .subscribe((event: ActivationEnd) => {
        if (event.snapshot && event.snapshot.component && this.constructor.name === event.snapshot.component['name']) {
          this.activationEndEmitter.next();
          this.helperService.backButtonSubscription = this.platform.backButton.subscribe(() => {
            this.goBack();
          });
        }
      });
  }

  goBack() {
    this.navCtrl.pop();
  }

  ngOnDestroy() {
    if (this.routerSubscription) {
      this.routerSubscription.unsubscribe();
      this.routerSubscription = null;
    }
    for (const observer of this.activationEndEmitter.observers) {
      observer.complete();
    }
  }

}
