import { CanBackPage } from './can-back.page';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed, async, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { NavController, Platform } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_CONFIG, SocketTesting } from 'logic-common';
import { Socket } from 'ngx-socket-io';

let wasPop = false;
const navCtrl = {
  pop: () => {
    wasPop = true;
  }
};
const platform = {
  backButton: of({})
};
describe('CanBackPage', () => {
  let router;
  let component: CanBackPage;
  let fixture: ComponentFixture<CanBackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CanBackPage],
      imports: [
        RouterTestingModule.withRoutes([{ path: 'can', children: [{ path: 'back', component: CanBackPage }] }]),
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: NavController,
          useValue: navCtrl,
        },
        {
          provide: Platform,
          useValue: platform,
        },
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanBackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
  });

  it('should navigate back when user click native "back" button', fakeAsync(() => {
    wasPop = false;
    router.navigate(['/can/back']);
    component.ngOnInit();
    tick();
    expect(wasPop).toBeTruthy();
    component.ngOnDestroy();
    expect(component).toBeTruthy();

    component.ngOnDestroy();
  }));
});
