export const environment = {
  production: true,
  API_URL: 'https://impacton.world/api',
  SOCKET_URL: 'https://impacton.world',
  STATIC_URL: 'https://impacton.world',
};
