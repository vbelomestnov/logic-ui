package world.impacton.logic;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.vk.api.sdk.VK;
import com.vk.api.sdk.VKApiCallback;
import com.vk.api.sdk.auth.VKAccessToken;
import com.vk.api.sdk.auth.VKScope;
import com.vk.api.sdk.exceptions.VKApiExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import world.impacton.logic.requests.VKWallPostRequest;

@NativePlugin()
public class VkOpenApi extends Plugin {

    @PluginMethod
    public void isLoggedIn(PluginCall call) {
        JSObject ret = new JSObject();
        ret.put("result", VK.isLoggedIn());
        call.success(ret);
    }

    @PluginMethod
    public void login(PluginCall call) {
        VK.login(this.getActivity(), Arrays.asList(VKScope.WALL));
        saveCall(call);
    }

    public void loginSuccess(VKAccessToken token) {
        PluginCall loginCall = getSavedCall();
        if (loginCall == null) {
            return;
        }
        JSObject ret = new JSObject();
        ret.put("token", token);
        loginCall.success(ret);
    }

    public void loginError(int errorCode) {
        PluginCall loginCall = getSavedCall();
        if (loginCall == null) {
            return;
        }
        loginCall.error(String.valueOf(errorCode));
    }

    @PluginMethod
    public void sharePost(PluginCall call) {
        String message = call.getString("message");
        String link = call.getString("link");
        VKApiCallback<JSONObject> callback = new VKApiCallback<JSONObject>() {
            @Override
            public void success(JSONObject result) {
                try {
                    JSObject ret = JSObject.fromJSONObject(result);
                    call.success(ret);
                } catch (JSONException e) {
                    call.error(e.getMessage());
                }
            }

            @Override
            public void fail(VKApiExecutionException error) {
                call.error(error.getErrorMsg());
            }
        };
        VK.execute(new VKWallPostRequest(message, link), callback);
    }

}
