package world.impacton.logic;

import android.content.Intent;
import android.os.Bundle;

import com.getcapacitor.BridgeActivity;
import com.getcapacitor.Plugin;
import com.vk.api.sdk.VK;
import com.vk.api.sdk.auth.VKAccessToken;
import com.vk.api.sdk.auth.VKAuthCallback;

import java.util.ArrayList;

public class MainActivity extends BridgeActivity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Initializes the Bridge
    this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
      // Additional plugins you've installed go here
      // Ex: add(TotallyAwesomePlugin.class);
      add(VkOpenApi.class);
    }});
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    VkOpenApi plugin = (VkOpenApi) bridge.getPlugin("VkOpenApi").getInstance();
    VKAuthCallback callback = new VKAuthCallback() {
      @Override
      public void onLogin(VKAccessToken token) {
        plugin.loginSuccess(token);
      }

      @Override
      public void onLoginFailed(int errorCode) {
        plugin.loginError(errorCode);
      }
    };
    if (!VK.onActivityResult(requestCode, resultCode, data, callback)) {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }
}
