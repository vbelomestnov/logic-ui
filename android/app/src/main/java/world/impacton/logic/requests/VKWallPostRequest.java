package world.impacton.logic.requests;

import com.vk.api.sdk.requests.VKRequest;

import org.json.JSONObject;


public class VKWallPostRequest extends VKRequest<JSONObject> {

    public VKWallPostRequest(String message, String link) {
        super(message);
        setMethod("wall.post");
        addParam("message", message);
        addParam("attachments", link);
    }

}
