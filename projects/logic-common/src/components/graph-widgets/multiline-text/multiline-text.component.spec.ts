import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultilineTextComponent } from './multiline-text.component';

describe('MultilineTextComponent', () => {
  let component: MultilineTextComponent;
  let fixture: ComponentFixture<MultilineTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultilineTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultilineTextComponent);
    component = fixture.componentInstance;
    component.text = 'asdf asdf asdfasd asd fasd fasd fasd fasdf asdf asdf asdf asdf asdf asdf asdf asdf asdffa asdf asdf asdf...';
    component.x = 0;
    component.y = 0;
    component.width = 200;
    component.height = 100;
    component.padding = 5;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('splitOnWords function - without splitters', () => {
    const word = '';
    const words: string[] = component.splitOnWords(word);
    expect(words.length).toEqual(0);
  });

  it('splitOnWords function', () => {
    const word = 'test';
    const words: string[] = component.splitOnWords(
      component.DELIMITERS.map(delimiter => word + delimiter + delimiter).join('') + ' '
    );
    expect(words.length).toEqual(component.DELIMITERS.length * 2 + 1);
  });

  it('lines splitting function - empty text', () => {
    component.text = '';
    expect(component.lines.length).toEqual(0);
  });

  it('lines splitting function', () => {
    expect(component.lines.length).toEqual(4);

    component.height = 50;
    expect(component.lines.length).toEqual(2);
  });

  it('left and top properties', () => {
    component.y = 10;
    expect(component.top).toEqual(component.y);
    component.x = 20;
    expect(component.left).toEqual(component.x + component.padding);
    expect(component.left).toBeGreaterThan(component.top);
  });

});
