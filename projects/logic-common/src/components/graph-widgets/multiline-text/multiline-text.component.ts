import { Component, OnInit, Input } from '@angular/core';
import { Size } from '../../../types/shapes/size';

const TEST_SVG_SIZE = '100';
const TEST_SVG_STRING_CLASS = 'test-svg-string';

@Component({
  selector: 'svg:g[app-multiline-text]',
  templateUrl: './multiline-text.component.html',
  styleUrls: ['./multiline-text.component.scss']
})
export class MultilineTextComponent implements OnInit {

  readonly DELIMITERS: string[] = [',', '.', '-', '!', '?', ';', '\n'];

  @Input() text: string;

  _x: number;
  get x(): number {
    return this._x;
  }
  @Input('x')
  set x(value: number) {
    this._x = +value;
  }

  _y: number;
  get y(): number {
    return this._y;
  }
  @Input('y')
  set y(value: number) {
    this._y = +value;
  }

  _width: number;
  get width(): number {
    return this._width;
  }
  @Input('width')
  set width(value: number) {
    this._width = +value;
  }

  _height: number;
  get height(): number {
    return this._height;
  }
  @Input('height')
  set height(value: number) {
    this._height = +value;
  }

  _padding: number;
  get padding(): number {
    return this._padding;
  }
  @Input('padding')
  set padding(value: number) {
    this._padding = +value;
  }

  lineHeight = 20;
  lineSpace = 2;

  constructor() { }

  ngOnInit() { }

  get lines(): string[] {
    const splittedText: string[] = this.splitOnWords(this.text);
    if (splittedText.length) {
      this.lineHeight = this.testLine(splittedText[0]).height;
    }

    let lines: string[] = [];
    let currentLine: string[] = [];
    for (const word of splittedText) {
      if (!currentLine.length) {
        currentLine.push(word);
      } else {
        const size: Size = this.testLine(currentLine.join() + word);
        if (size.width + this.padding < this.width) {
          currentLine.push(word);
        } else {
          lines.push(currentLine.join(''));
          currentLine = [word];
        }
      }
    }
    if (currentLine.length) {
      lines.push(currentLine.join(''));
    }

    if (lines.length * (this.lineHeight + this.lineSpace) + this.padding > this.height) {
      const expectedLinesCount: number = Math.trunc((this.height - this.padding) / (this.lineHeight + this.lineSpace)) + 1;
      lines = lines.slice(0, expectedLinesCount - 1);
    }

    return lines;
  }

  get left(): number {
    return this.x + this.padding;
  }

  get top(): number {
    return this.y;
  }

  splitOnWords(text: string): string[] {
    // spaces
    let result: string[] = text.split(' ').map(aText => aText + ' ');
    // If splitter at the end of test eg 'asdf ' then last splitted string will be empty
    if (result[result.length - 1].length === 1) {
      result = result.slice(0, result.length - 1);
    } else {
      result[result.length - 1] = result[result.length - 1].substring(0, result[result.length - 1].length - 1);
    }

    // other delimiters
    for (const delimiter of this.DELIMITERS) {
      result = result.map((aText: string): string[] => {
        let arr: string[] = aText.split(delimiter).map(splittedText => splittedText + delimiter);
        // If splitter at the end of test eg 'asdf ' then last splitted string will be empty
        if (arr[arr.length - 1].length === 1) {
          arr = arr.slice(0, arr.length - 1);
        } else {
          arr[arr.length - 1] = arr[arr.length - 1].substring(0, arr[arr.length - 1].length - 1);
        }
        return arr;
      }).reduce((accum: string[], value: string[]) => accum.concat(value), []);
    }

    return result;
  }

  testLine(line: string): Size {
    if (typeof document === 'undefined') {
      return {
        height: 0,
        width: 0,
      };
    }
    const svg = document.createElement('svg');
    svg.setAttribute('width', TEST_SVG_SIZE);
    svg.setAttribute('height', TEST_SVG_SIZE);
    svg.innerHTML = `<text class="${TEST_SVG_STRING_CLASS}" x="0" y="0">
      ${line}
    </text>`;
    document.body.prepend(svg);
    const text: HTMLElement = svg.getElementsByClassName(TEST_SVG_STRING_CLASS)[0] as HTMLElement;
    const result = {
      height: text.offsetHeight,
      width: text.offsetWidth
    };
    document.body.removeChild(svg);
    return result;
  }

}
