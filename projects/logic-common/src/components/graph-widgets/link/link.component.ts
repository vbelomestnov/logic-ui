import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Link } from '../../../types/shapes/link';
import { GraphMode } from '../../../types/enums/graph-mode';

@Component({
  selector: 'svg:g[app-link]',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss'],
  animations: [
    trigger('distinguishLink', [
      state('true', style({ fill: 'orange', stroke: 'orange', })),
      state('false', style({ fill: '*', stroke: '*', strokeWidth: '*', })),
      transition(':enter', []),
      transition('* => *', animate('300ms ease')),
    ]),
  ]
})
export class LinkComponent implements OnInit {

  GraphMode = GraphMode;

  @Input() link: Link;
  @Input() mode: GraphMode;

  constructor() { }

  ngOnInit() {}

}
