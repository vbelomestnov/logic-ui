import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RectTextComponent } from './rect-text.component';
import { MultilineTextComponent } from '../multiline-text/multiline-text.component';
import { Rectangle } from '../../../types/shapes/rectangle';
import { ShapeType } from '../../../types/enums/shape-type';
import { DragType } from '../../../types/enums/drag-type';
import { ShapeDragEvent } from '../../../types/shapes/shape-drag-event';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GraphMode } from '../../../types/enums/graph-mode';

const RECT1: Rectangle = {
  name: 'object1',
  type: ShapeType.rectangle,
  x: 100,
  y: 100,
  width: 200,
  height: 100,
  text: 'asdf asdf asdfasd asd fasd fasd fasd fasdf asdf asdf asdf asdf asdf asdf asdf asdf asdffa asdf asdf asdf...',
  placement: [],
  selfPlacement: [],
};

describe('RectTextComponent', () => {
  let component: RectTextComponent;
  let fixture: ComponentFixture<RectTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RectTextComponent, MultilineTextComponent],
      imports: [BrowserAnimationsModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RectTextComponent);
    component = fixture.componentInstance;
    component.data = RECT1;
    component.mode = GraphMode.resize;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('start dragging by mouse', () => {
    let dragEvent: ShapeDragEvent;
    component.movementStart.subscribe(aDragEvent => dragEvent = aDragEvent);
    component.mouseDown(new MouseEvent('mousedown', { clientX: 105, clientY: 105 }), DragType.movement);
    expect(dragEvent.shapeName).toEqual(component.data.name);
    expect(dragEvent.dragType).toEqual(DragType.movement);
    expect(dragEvent.x).toEqual(105);
    expect(dragEvent.y).toEqual(105);
  });

  it('start dragging by touchpad', () => {
    let dragEvent: ShapeDragEvent;
    component.movementStart.subscribe(aDragEvent => dragEvent = aDragEvent);
    component.touchStart(new TouchEvent('touchstart', {
      touches: [new Touch({
        identifier: 0, target: new EventTarget(), clientX: 105, clientY: 105
      })]
    }), DragType.movement);
    expect(dragEvent.shapeName).toEqual(component.data.name);
    expect(dragEvent.dragType).toEqual(DragType.movement);
    expect(dragEvent.x).toEqual(105);
    expect(dragEvent.y).toEqual(105);
  });

  it('rectangle can has different fill', () => {
    component.mode = GraphMode.resize;
    expect(component.fill).toBe('yellow');
    component.mode = GraphMode.disabled;
    expect(component.fill).toBe('grey');
    component.mode = undefined;
    expect(component.fill).toBe('white');
  });

  it('can start movement/resizing only if mode is "resize"', () => {
    let movementData = null;
    component.mode = undefined;
    component.movementStart.subscribe(data => movementData = data);
    component.mouseDown(new MouseEvent('mousedown', { clientX: 105, clientY: 105 }), DragType.movement);
    expect(movementData).toBeNull();
    component.mode = GraphMode.resize;
    component.mouseDown(new MouseEvent('mousedown', { clientX: 105, clientY: 105 }), DragType.movement);
    expect(movementData).not.toBeNull();

    component.mode = undefined;
    movementData = null;
    component.touchStart(new TouchEvent('touchstart', {
      touches: [new Touch({
        identifier: 0, target: new EventTarget(), clientX: 105, clientY: 105
      })]
    }), DragType.movement);
    expect(movementData).toBeNull();
    component.mode = GraphMode.resize;
    component.touchStart(new TouchEvent('touchstart', {
      touches: [new Touch({
        identifier: 0, target: new EventTarget(), clientX: 105, clientY: 105
      })]
    }), DragType.movement);
    expect(movementData).not.toBeNull();
  });
});
