import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DragType } from '../../../types/enums/drag-type';
import { Rectangle } from '../../../types/shapes/rectangle';
import { ShapeDragEvent } from '../../../types/shapes/shape-drag-event';
import { trigger, transition, animate, style, state } from '@angular/animations';
import { GraphMode } from '../../../types/enums/graph-mode';

@Component({
  selector: 'svg:g[app-rect-text]',
  templateUrl: './rect-text.component.html',
  styleUrls: ['./rect-text.component.scss'],
  animations: [
    trigger('distinguishShape', [
      state('true', style({ fill: 'yellow', stroke: 'orange', strokeWidth: 2, })),
      state('false', style({ fill: '*', stroke: '*', strokeWidth: '*', })),
      transition(':enter', []),
      transition('* => *', animate('300ms ease')),
    ]),
  ]
})
export class RectTextComponent implements OnInit {

  GraphMode = GraphMode;

  readonly DragType = DragType;
  @Input() data: Rectangle;
  @Input() mode: GraphMode;
  @Output() movementStart: EventEmitter<ShapeDragEvent> = new EventEmitter();

  get canResize(): boolean {
    return (this.mode === GraphMode.resize);
  }

  get fill() {
    if (this.mode === GraphMode.resize) {
      return 'yellow';
    } else if (this.mode === GraphMode.disabled) {
      return 'grey';
    }
    return 'white';
  }

  constructor() { }

  ngOnInit() {
  }

  mouseDown(event: MouseEvent, dragType: DragType): void {
    if (!this.canResize) {
      return;
    }
    const x = event.clientX;
    const y = event.clientY;
    this.movementStart.emit({ shapeName: this.data.name, dragType, x, y });
    event.stopPropagation();
  }

  touchStart(event: TouchEvent, dragType: DragType): void {
    if (!this.canResize) {
      return;
    }
    const x = event.touches[0].clientX;
    const y = event.touches[0].clientY;
    this.movementStart.emit({ shapeName: this.data.name, dragType, x, y });
    event.stopPropagation();
  }

}
