import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DragType } from '../../types/enums/drag-type';
import { Shape } from '../../types/shapes/shape';
import { ShapeType } from '../../types/enums/shape-type';
import { Rectangle } from '../../types/shapes/rectangle';
import { ShapeDragEvent } from '../../types/shapes/shape-drag-event';
import { Link } from '../../types/shapes/link';
import { GraphService } from '../../services/graph.service';
import { GraphData } from '../../types/graph-data';
import { trigger, transition, sequence, query, animate, style } from '@angular/animations';
import { GraphMode } from '../../types/enums/graph-mode';
import { fromJS, is } from 'immutable';
import { GraphCalcService } from '../../services/graph-calc.service';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss'],
  animations: [
    trigger('cover', [
      transition(':increment', [
        query('g.app-rect-text.covered', [
          sequence([
            animate('80ms ease', style({
              transform: 'translateX(-5px)',
            })),
            animate('80ms ease', style({
              transform: 'translateX(5px)',
            })),
            animate('80ms ease', style({
              transform: 'translateX(-5px)',
            })),
            animate('80ms ease', style({
              transform: 'translateX(5px)',
            })),
            animate('80ms ease', style({
              transform: 'translateX(-5px)',
            })),
            animate('80ms ease', style({
              transform: '*',
            })),
          ])
        ], { optional: true })
      ])
    ]),
    trigger('outOfBounds', [
      transition(':increment', [
        query('.crossed', [
          sequence([
            animate('80ms ease', style({
              strokeWidth: 5,
            })),
            animate('80ms ease', style({
              strokeWidth: 1,
            })),
            animate('80ms ease', style({
              strokeWidth: 5,
            })),
            animate('80ms ease', style({
              strokeWidth: 1,
            })),
            animate('80ms ease', style({
              strokeWidth: 5,
            })),
            animate('80ms ease', style({
              strokeWidth: '*',
            })),
          ])
        ], { optional: true })
      ])
    ]),
  ],
})
export class GraphComponent implements OnInit {

  GraphMode = GraphMode;

  @Input() data: GraphData = null;
  @Input() mode: GraphMode;
  @Output() shapeCreated: EventEmitter<Shape> = new EventEmitter();
  @Output() shapeClick: EventEmitter<{ shape: Shape, event: MouseEvent }> = new EventEmitter();
  @Output() linkClick: EventEmitter<{ link: Link, event: MouseEvent }> = new EventEmitter();

  @ViewChild('rect', { static: false }) rectElement;
  @ViewChild('svg', { static: false }) svgElement;

  lastX: number = null;
  lastY: number = null;
  resizingShape: Shape = null;
  immOriginShape = null;
  dragType: DragType = null;
  hasLinkOverlap = false;

  coverAnimationCounter = 0;

  constructor(public graphService: GraphService, private calcService: GraphCalcService) {
    this.calcService.listenGraph()
      .subscribe(() => {
        this.resizingShape = null;
      });
  }

  ngOnInit() { }

  movementStart({ shapeName, dragType, x, y }: ShapeDragEvent): void {
    if (this.mode === GraphMode.resize || this.graphService.resizingShapeName === shapeName) {
      this.lastX = x;
      this.lastY = y;
      this.resizingShape = this.data.shapes.find((shape: Shape) => shape.name === shapeName);
      this.dragType = dragType;
      this.immOriginShape = fromJS(this.resizingShape);
    }
  }

  resizeEnd(): void {
    if (this.resizingShape) {
      if (!is(this.immOriginShape, fromJS(this.resizingShape))) {
        this.graphService.setShapePlacements(this.resizingShape)
          .then(() => {
            this.immOriginShape = null;
            this.lastX = null;
            this.lastY = null;
            this.dragType = null;
          })
          .catch((e) => {
            this.immOriginShape = null;
            this.lastX = null;
            this.lastY = null;
            this.dragType = null;
            this.coverAnimationCounter++;
          });
      } else {
        this.immOriginShape = null;
        this.lastX = null;
        this.lastY = null;
        this.dragType = null;
      }
      this.resizingShape = null;
    }
  }

  moveLine(dx: number, dy: number): void {
    this.graphService.resetLinks();
    switch (this.dragType) {
      case DragType.resizeY:
        switch (this.resizingShape.type) {
          case ShapeType.rectangle:
            const rect: Rectangle = this.resizingShape as Rectangle;
            rect.height = rect.height + dy;
            break;
        }
        break;
      case DragType.resizeX:
        switch (this.resizingShape.type) {
          case ShapeType.rectangle:
            const rect: Rectangle = this.resizingShape as Rectangle;
            rect.width = rect.width + dx;
            break;
        }
        break;
      default:
        this.resizingShape.x = this.resizingShape.x + dx;
        this.resizingShape.y = this.resizingShape.y + dy;
    }
  }

  mouseMove(event: MouseEvent): void {
    if (this.resizingShape) {
      const dx = event.clientX - this.lastX;
      const dy = event.clientY - this.lastY;
      this.lastX = event.clientX;
      this.lastY = event.clientY;
      this.moveLine(dx, dy);
    }
  }

  mouseUp(event: MouseEvent): void {
    if (!this.resizingShape && this.graphService.newShapes.length) {
      const endX = event.clientX;
      const endY = event.clientY;
      const rect = this.rectElement.nativeElement.getBoundingClientRect();
      const widthRatio = this.data.width / rect.width;
      const heightRatio = this.data.height / rect.height;
      this.graphService.newShape((endX - rect.x) * widthRatio, (endY - rect.y) * heightRatio)
        .then((newShape: Shape) => {
          this.shapeCreated.emit(newShape);
        })
        .catch(() => {
          this.coverAnimationCounter++;
        });
    }
    this.resizeEnd();
  }

  touchMove(event: TouchEvent): void {
    if (this.resizingShape) {
      const dx = event.touches[0].clientX - this.lastX;
      const dy = event.touches[0].clientY - this.lastY;
      this.lastX = event.touches[0].clientX;
      this.lastY = event.touches[0].clientY;
      this.moveLine(dx, dy);
      event.preventDefault();
    }
  }

  touchEnd(event: TouchEvent): void {
    this.resizeEnd();
  }

  clickShapeHandler(shape: Shape, event) {
    if (!this.mode || this.mode === GraphMode.preview) {
      this.graphService.distinguishShape(shape);
    }
    if (!this.immOriginShape || is(this.immOriginShape, fromJS(shape))) {
      this.shapeClick.emit({ shape, event });
    }
  }

  clickLinkHandler(link: Link, event) {
    if (!this.mode || this.mode === GraphMode.preview) {
      this.graphService.distinguishLink(link);
    }
    this.linkClick.emit({ link, event });
  }

  getRectangleMode(shape: Shape): GraphMode {
    if (this.mode === GraphMode.preview) {
      return GraphMode.preview;
    }
    if (this.graphService.newLinks.length) {
      const newLink = this.graphService.newLinks[0];
      const firstShapeName = newLink.source || newLink.dest;
      if (shape.name === firstShapeName) {
        return GraphMode.disabled;
      }
    }
    if (shape.name === this.graphService.resizingShapeName || this.mode === GraphMode.resize) {
      return GraphMode.resize;
    }
    return undefined;
  }

  shapeTrackBy(index: number, shape: Shape): string {
    return shape.name;
  }

}
