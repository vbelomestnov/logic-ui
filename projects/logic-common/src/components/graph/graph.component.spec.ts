import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { GraphComponent } from './graph.component';
import { RectTextComponent } from '../graph-widgets/rect-text/rect-text.component';
import { MultilineTextComponent } from '../graph-widgets/multiline-text/multiline-text.component';
import { DragType } from '../../types/enums/drag-type';
import { Rectangle } from '../../types/shapes/rectangle';
import { graphStubData, GraphService } from '../../services/graph.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LinkComponent } from '../graph-widgets/link/link.component';
import { Shape } from '../../types/shapes/shape';
import { GraphMode } from '../../types/enums/graph-mode';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_CONFIG } from '../../app.config';
import { Socket } from 'ngx-socket-io';
import { SocketTesting } from '../../services/graph-calc.service.mock.spec';

describe('GraphComponent', () => {
  let component: GraphComponent;
  let fixture: ComponentFixture<GraphComponent>;
  let graphService: GraphService;
  let socket: SocketTesting;

  const rectangleMove = (rect: Rectangle, dx: number, dy: number, eventType: string, shouldIntersect?: string): void => {
    const xPostion = rect.x + Math.trunc(rect.width / 2);
    const yPostion = rect.y + Math.trunc(rect.height / 2);
    component.movementStart({
      shapeName: rect.name,
      dragType: DragType.movement,
      x: xPostion,
      y: yPostion
    });
    switch (eventType) {
      case 'touchmove':
        component.touchMove(new TouchEvent('touchmove', {
          touches: [new Touch({
            identifier: 0, target: new EventTarget(), clientX: xPostion + dx, clientY: yPostion + dy
          })]
        }));
        break;
      case 'mousemove':
        component.mouseMove(new MouseEvent('mousemove', { clientX: xPostion + dx, clientY: yPostion + dy }));
        break;
    }
    switch (eventType) {
      case 'mousemove':
        component.mouseUp(new MouseEvent('mouseup', { clientX: xPostion, clientY: rect.y + rect.height + dy }));
        break;
      case 'touchmove':
        component.touchEnd(new TouchEvent('touchend', {
          changedTouches: [new Touch({
            identifier: 0, target: new EventTarget(), clientX: xPostion, clientY: rect.y + rect.height + dy
          })]
        }));
        break;
    }
  };

  const rectangleResizeY = (rect: Rectangle, dy: number, eventType: string, shouldIntersect?: string): void => {
    const xPostion = rect.x + Math.trunc(rect.width / 2);
    component.movementStart({
      shapeName: rect.name,
      dragType: DragType.resizeY,
      x: xPostion,
      y: rect.y + rect.height
    });
    switch (eventType) {
      case 'mousemove':
        component.mouseMove(new MouseEvent('mousemove', { clientX: xPostion, clientY: rect.y + rect.height + dy }));
        break;
      case 'touchmove':
        component.touchMove(new TouchEvent('touchmove', {
          touches: [new Touch({
            identifier: 0, target: new EventTarget(), clientX: xPostion, clientY: rect.y + rect.height + dy
          })]
        }));
        break;
    }
    switch (eventType) {
      case 'mousemove':
        component.mouseUp(new MouseEvent('mouseup', { clientX: xPostion, clientY: rect.y + rect.height + dy }));
        break;
      case 'touchmove':
        component.touchEnd(new TouchEvent('touchend', {
          changedTouches: [new Touch({
            identifier: 0, target: new EventTarget(), clientX: xPostion, clientY: rect.y + rect.height + dy
          })]
        }));
        break;
    }
  };

  const rectangleResizeX = (rect: Rectangle, dx: number, eventType: string): void => {
    const yPostion = rect.y + Math.trunc(rect.height / 2);
    component.movementStart({
      shapeName: rect.name,
      dragType: DragType.resizeX,
      x: rect.x + rect.width,
      y: yPostion
    });
    switch (eventType) {
      case 'mousemove':
        component.mouseMove(new MouseEvent('mousemove', { clientX: rect.x + rect.width + dx, clientY: yPostion }));
        break;
      case 'touchmove':
        component.touchMove(new TouchEvent('touchmove', {
          touches: [new Touch({
            identifier: 0, target: new EventTarget(), clientX: rect.x + rect.width + dx, clientY: yPostion
          })]
        }));
        break;
    }
    component.resizeEnd();
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GraphComponent, RectTextComponent, MultilineTextComponent, LinkComponent],
      imports: [RouterTestingModule, BrowserAnimationsModule, HttpClientTestingModule],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        { provide: Socket, useClass: SocketTesting },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    graphService = TestBed.get(GraphService);
    socket = TestBed.get(Socket);
    graphService.data = graphStubData();
    fixture = TestBed.createComponent(GraphComponent);
    component = fixture.componentInstance;
    component.data = graphService.data;
    component.mode = GraphMode.resize;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('mouse move without start', () => {
    const shape = component.data.shapes[0] as Rectangle;
    const initX = shape.x;
    const initY = shape.y;
    const initWidth = shape.width;
    const initHeight = shape.height;
    const xPostion = shape.x + Math.trunc(shape.width / 2);
    const yPostion = shape.y + Math.trunc(shape.height / 2);
    component.mouseMove(new MouseEvent('mousemove', { clientX: xPostion - 10, clientY: yPostion + 10 }));
    component.resizeEnd();
    expect(shape.x).toEqual(initX);
    expect(shape.y).toEqual(initY);
    expect(shape.width).toEqual(initWidth);
    expect(shape.height).toEqual(initHeight);
  });

  it('touchpad move without start', () => {
    const shape = component.data.shapes[0] as Rectangle;
    const initX = shape.x;
    const initY = shape.y;
    const initWidth = shape.width;
    const initHeight = shape.height;
    const xPostion = shape.x + Math.trunc(shape.width / 2);
    const yPostion = shape.y + Math.trunc(shape.height / 2);
    component.touchMove(new TouchEvent('touchmove', {
      touches: [new Touch({
        identifier: 0, target: new EventTarget(), clientX: xPostion - 10, clientY: yPostion + 10
      })]
    }));
    component.resizeEnd();
    expect(shape.x).toEqual(initX);
    expect(shape.y).toEqual(initY);
    expect(shape.width).toEqual(initWidth);
    expect(shape.height).toEqual(initHeight);
  });


  it('rectangle resize vertical', () => {
    const shape = component.data.shapes[0] as Rectangle;
    const initHeight = shape.height;
    rectangleResizeY(shape, 10, 'mousemove');
    expect(shape.height).toBeGreaterThan(initHeight);
  });

  it('rectangle resize horizontal', () => {
    const shape = component.data.shapes[0] as Rectangle;
    const initWidth = shape.width;
    rectangleResizeX(shape, -10, 'mousemove');
    expect(shape.width).toBeLessThan(initWidth);
  });

  it('rectangle movement', () => {
    const shape = component.data.shapes[0] as Rectangle;
    const initX = shape.x;
    const initY = shape.y;
    rectangleMove(shape, -10, 10, 'touchmove');
    expect(shape.x).toBeLessThan(initX);
    expect(shape.y).toBeGreaterThan(initY);
  });

  it('graph should allow movement/resizing a shape if this shape marked in data as "resizingShapeName"', () => {
    const shape = component.data.shapes[0] as Rectangle;
    component.mode = undefined;
    const initX = shape.x;
    const initY = shape.y;
    rectangleMove(shape, -10, 10, 'touchmove');
    expect(shape.x).toBe(initX);
    expect(shape.y).toBe(initY);
    component.graphService.toggleResizing(shape.name);
    rectangleMove(shape, -10, 10, 'touchmove');
    expect(shape.x).toBeLessThan(initX);
    expect(shape.y).toBeGreaterThan(initY);
    component.graphService.toggleResizing(shape.name);
  });

  it('graph should allow movement/resizing a shape if this shape marked in data as "resizingShapeName"', () => {
    const shape = component.data.shapes[0] as Rectangle;
    component.mode = undefined;
    const initX = shape.x;
    const initY = shape.y;
    rectangleMove(shape, -10, 10, 'touchmove');
    expect(shape.x).toBe(initX);
    expect(shape.y).toBe(initY);
    component.graphService.toggleResizing(shape.name);
    rectangleMove(shape, -10, 10, 'touchmove');
    expect(shape.x).toBeLessThan(initX);
    expect(shape.y).toBeGreaterThan(initY);
    component.graphService.toggleResizing(shape.name);
  });

  it('should change shape placement after move/resize action and then mouseup', fakeAsync(() => {
    const shape = component.data.shapes[0] as Rectangle;
    const initCounter = component.coverAnimationCounter;
    component.mode = undefined;
    component.graphService.toggleResizing(shape.name);
    socket._exceptionMessage = null;
    socket._graphSuccess = true;
    rectangleMove(shape, -10, 10, 'touchmove');
    tick();
    expect(component.coverAnimationCounter).toBe(initCounter);
    socket._exceptionMessage = 'Test error';
    socket._graphSuccess = false;
    rectangleMove(shape, -10, 10, 'touchmove');
    tick();
    expect(component.coverAnimationCounter).toBe(initCounter + 1);
  }));

  it('setting up position of new shape', fakeAsync(() => {
    const text = 'test new shape';
    const initCounter = component.coverAnimationCounter;
    component.graphService.queueNewShape(text);
    socket._exceptionMessage = null;
    socket._graphSuccess = true;
    component.mouseUp(new MouseEvent('mouseup', { clientX: 70, clientY: 300 }));
    tick();
    expect(component.coverAnimationCounter).toBe(initCounter);
    component.graphService.queueNewShape(text);
    socket._exceptionMessage = 'Test error';
    socket._graphSuccess = false;
    component.mouseUp(new MouseEvent('mouseup', { clientX: 70, clientY: 300 }));
    tick();
    expect(component.coverAnimationCounter).toBe(initCounter + 1);
  }));

  it('distinguiging objects on click (preview and editor mode)', () => {
    const shape = component.data.shapes[0] as Rectangle;
    let emittedShape = null;
    component.shapeClick.subscribe((aShape: Shape) => emittedShape = aShape);
    component.clickShapeHandler(shape, new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(emittedShape).not.toBeNull();
    expect(shape.distinguished).toBe(true);

    const link = component.data.links[0];
    component.mode = undefined;
    let emittedLink = null;
    component.linkClick.subscribe((aLink: Shape) => emittedLink = aLink);
    component.clickLinkHandler(link, new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(emittedLink).not.toBeNull();
    expect(link.distinguished).toBe(true);
    component.mode = GraphMode.preview;

    component.mode = GraphMode.preview;
    emittedShape = null;
    component.clickShapeHandler(shape, new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(emittedShape).not.toBeNull();
    expect(shape.distinguished).toBe(true);

    component.mode = GraphMode.disabled;
    emittedLink = null;
    component.clickLinkHandler(link, new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(link.distinguished).not.toBe(true);
    expect(shape.distinguished).toBe(true);

    component.mode = null;
    emittedLink = null;
    component.clickLinkHandler(link, new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(emittedLink).not.toBeNull();
    expect(link.distinguished).toBe(true);
    expect(shape.distinguished).not.toBe(true);

    component.mode = GraphMode.disabled;
    emittedShape = null;
    component.clickShapeHandler(shape, new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(link.distinguished).toBe(true);
    expect(shape.distinguished).not.toBe(true);
  });

  it('should prevent emitting "shapeClick" if shape is in movement', () => {
    component.mode = undefined;
    const shape = component.data.shapes[0] as Rectangle;
    graphService.toggleResizing(shape.name);
    let emittedShape = null;
    component.shapeClick.subscribe((aShape: Shape) => emittedShape = aShape);
    component.movementStart({ shapeName: shape.name, dragType: DragType.movement, x: -1, y: -1 });
    rectangleMove(shape, -10, 10, 'touchmove');
    component.clickShapeHandler(shape, new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(emittedShape).toBeNull();

    component.movementStart({ shapeName: shape.name, dragType: DragType.movement, x: -1, y: -1 });
    graphService.distinguishShape(shape);
    component.clickShapeHandler(shape, new MouseEvent('mouseclick', { clientX: 290, clientY: 100 }));
    expect(emittedShape).not.toBeNull();
  });

  it('should not request shape placement just after a click', () => {
    const spySetShapePlacement = spyOn(graphService, 'setShapePlacements');
    component.mode = undefined;
    const shape = component.data.shapes[0] as Rectangle;
    graphService.toggleResizing(shape.name);
    graphService.distinguishShape(shape);
    component.movementStart({ shapeName: shape.name, dragType: DragType.movement, x: 0, y: 0 });
    component.mouseUp(new MouseEvent('mouseup', { clientX: 290, clientY: 100 }));
    expect(spySetShapePlacement).not.toHaveBeenCalled();
  });

  it('define mode for rectangle', () => {
    const shape = component.data.shapes[0] as Rectangle;
    const shape1 = component.data.shapes[1] as Rectangle;
    component.mode = GraphMode.preview;
    expect(component.getRectangleMode(shape)).toBe(GraphMode.preview);
    component.mode = undefined;
    component.graphService.queueNewLink(shape.name, 'source');
    expect(component.getRectangleMode(shape)).toBe(GraphMode.disabled);
    graphService.refuseNewLinks();
    component.graphService.queueNewLink(shape.name, 'dest');
    expect(component.getRectangleMode(shape)).toBe(GraphMode.disabled);
    expect(component.getRectangleMode(shape1)).toBe(undefined);
    graphService.refuseNewLinks();
    expect(component.getRectangleMode(shape)).toBe(undefined);
  });

  it('should not change shape placement if new graph data has been received', fakeAsync(() => {
    const shape = component.data.shapes[0] as Rectangle;
    const initWidth = shape.width;
    const initCounter = component.coverAnimationCounter;
    component.mode = undefined;
    component.graphService.toggleResizing(shape.name);
    socket._exceptionMessage = null;
    socket._graphSuccess = true;
    const yPostion = shape.y + Math.trunc(shape.height / 2);
    component.movementStart({
      shapeName: shape.name,
      dragType: DragType.resizeX,
      x: shape.x + shape.width,
      y: yPostion
    });
    socket._emitGraph(graphStubData());
    component.touchMove(new TouchEvent('touchmove', {
      touches: [new Touch({
        identifier: 0, target: new EventTarget(), clientX: shape.x + shape.width + -10, clientY: yPostion
      })]
    }));
    component.resizeEnd();
    tick();
    expect(component.coverAnimationCounter).toBe(initCounter);
    expect((component.data.shapes[0] as Rectangle).width).toBe(initWidth);
  }));
});
