import { NgModule } from '@angular/core';
import { GraphComponent } from './graph/graph.component';
import { RectTextComponent } from './graph-widgets/rect-text/rect-text.component';
import { MultilineTextComponent } from './graph-widgets/multiline-text/multiline-text.component';
import { CommonModule } from '@angular/common';
import { LinkComponent } from './graph-widgets/link/link.component';

@NgModule({
  declarations: [
    RectTextComponent,
    MultilineTextComponent,
    LinkComponent,
    GraphComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    RectTextComponent,
    MultilineTextComponent,
    LinkComponent,
    GraphComponent,
  ],
})
export class GraphModule {}
