import { InjectionToken } from '@angular/core';
import { AppConfig } from './types/app-config';

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
