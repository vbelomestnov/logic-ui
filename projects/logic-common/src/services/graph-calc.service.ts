import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Shape } from '../types/shapes/shape';
import { GraphServerData } from '../types/graph-data';
import { Link } from '../types/shapes/link';

@Injectable({
  providedIn: 'root'
})
export class GraphCalcService {

  private _state = 'empty';
  public get state(): string {
    return this._state;
  }

  public get socketId(): string {
    return this.socket.ioSocket.id;
  }

  constructor(private socket: Socket) {
    socket.fromEvent('state')
      .subscribe((state: string) => {
        // console.log(state);
        this._state = state;
      });
    socket.fromEvent('exception')
      .subscribe((error) => console.error(error));
  }

  listenGraph() {
    return this.socket.fromEvent('graph');
  }

  listenException() {
    return this.socket.fromEvent('exception');
  }

  listenOutOfBoard() {
    return this.socket.fromEvent('outOfBoard');
  }

  listenReconnect() {
    return this.socket.fromEvent('reconnect');
  }

  initGraph(code: string) {
    this._state = 'empty';
    this.socket.emit('initGraph', code);
  }

  addShape(shape: Shape): Promise<Shape> {
    return new Promise((resolve, reject) => {
      this.socket.fromOneTimeEvent('graph')
        .then((result: { success: boolean, graph: GraphServerData }) => {
          if (result.success) {
            resolve(shape);
          }
        });
      this.socket.fromOneTimeEvent('exception')
        .then((error) => {
          reject(error);
        });
      this.socket.emit('addShape', shape);
    });
  }

  removeShape(shapeName: string) {
    this.socket.emit('removeShape', shapeName);
  }

  addLink(link: Link): Promise<Link> {
    return new Promise((resolve, reject) => {
      this.socket.fromOneTimeEvent('graph')
        .then((result: { success: boolean, graph: GraphServerData }) => {
          if (result.success) {
            resolve(link);
          }
        });
      this.socket.fromOneTimeEvent('exception')
        .then((error) => {
          reject(error);
        });
      this.socket.emit('addLink', link);
    });
  }

  removeLink(source: string, dest: string) {
    this.socket.emit('removeLink', { source, dest });
  }

  async setShapePlacements(shape: Shape): Promise<Shape> {
    return new Promise((resolve, reject) => {
      this.socket.fromOneTimeEvent('graph')
        .then((result: { success: boolean, graph: GraphServerData }) => {
          if (result.success) {
            resolve();
          }
        });
      this.socket.fromOneTimeEvent('exception')
        .then((error) => {
          reject(error);
        });
      this.socket.emit('setShapePlacements', shape);
    });
  }

  saveTitle(title: string) {
    this.socket.emit('saveTitle', title);
  }

  saveShapeText(shapeName: string, text: string): void {
    this.socket.emit('saveShapeText', { shapeName, text });
  }

  breakCalculations() {
    this.socket.emit('break');
  }

}
