import { Injectable } from '@angular/core';
import { Rectangle } from '../types/shapes/rectangle';
import { ShapeType } from '../types/enums/shape-type';
import { GraphData, GraphServerData } from '../types/graph-data';
import { Shape } from '../types/shapes/shape';
import { Link } from '../types/shapes/link';
import { RestApiService } from './rest-api.service';
import { Observable, Subscriber } from 'rxjs';
import { fromJS, is } from 'immutable';
import { GraphCalcService } from './graph-calc.service';
import { GraphError } from '../types/graph-error';

@Injectable({
  providedIn: 'root'
})
export class GraphService {

  static defaultHeight = 600;
  static defaultWidth = 400;

  private objectsCounter = 0;
  public graphCounter = 0;

  private isMovingShapeOutOfBoard = false;
  get isOutOfBoard(): boolean {
    return this.isMovingShapeOutOfBoard;
  }

  get hasChanged(): boolean {
    const data = this.data;
    for (const shape of data.shapes) {
      delete shape.distinguished;
      delete shape.isCovered;
      delete shape.placement;
      delete shape.selfPlacement;
    }

    for (const link of data.links) {
      delete link.distinguished;
    }

    const immData = fromJS(data);
    return !is(immData, this.immInitData);
  }

  data: GraphData = null;
  newShapes: Shape[] = [];
  newLinks: Link[] = [];
  resizingShapeName: string;
  immInitData = null;

  constructor(
    private restApiService: RestApiService,
    private graphCalcService: GraphCalcService,
  ) {
    this.graphCalcService.listenGraph()
      .subscribe((result: { success: boolean, graph: GraphServerData }) => {
        this.graphCounter++;
        this.initGraph(result.graph);
        if (result.success) {
          this.newShapes = [];
          this.newLinks = [];
        }
      });

    this.graphCalcService.listenOutOfBoard()
      .subscribe((isOutOfBoard: boolean) => {
        this.graphCounter++;
        this.isMovingShapeOutOfBoard = isOutOfBoard;
      });

    this.graphCalcService.listenReconnect()
      .subscribe(() => {
        if (this.data) {
          this.loadGraph(this.data.code || 'new').subscribe((result) => {
            this.graphCalcService.initGraph(result.code || 'new');
          });
        }
      });
  }

  newGraph() {
    this.objectsCounter = 0;
    this.data = {
      width: GraphService.defaultWidth,
      height: GraphService.defaultHeight,
      code: '',
      title: '',
      shapes: [],
      links: []
    };
    this.immInitData = fromJS(this.data);
    this.graphCalcService.initGraph('new');
  }

  loadGraph(code: string): Observable<GraphData> {
    return new Observable<GraphData>((subscriber: Subscriber<GraphData>) => {
      this.restApiService.getGraph(code)
        .subscribe(
          (result: GraphServerData) => {
            this.initGraph(result);
            this.graphCalcService.initGraph(result.code);
            subscriber.next(this.data);
          },
          (error) => subscriber.error(error)
        );
    });
  }

  public saveGraph(svg: string): Observable<GraphData> {
    return new Observable<GraphData>((subscriber: Subscriber<GraphData>) => {
      this.restApiService.saveGraph({socketId: this.graphCalcService.socketId, svg})
        .subscribe(
          (result: GraphServerData) => {
            this.initGraph(result);
            subscriber.next(this.data);
          },
          (error) => subscriber.error(error)
        );
    });
  }

  private initGraph(graph: GraphServerData) {
    const names = graph.shapes.map((shape: Shape) => {
      const nameParts = shape.name.split('-');
      if (nameParts[1]) {
        return +nameParts[1];
      } else {
        return 0;
      }
    });
    this.objectsCounter = names.reduce((accum, value) => {
      if (value > accum) {
        return value;
      } else {
        return accum;
      }
    }, 0);
    this.data = {
      ...graph,
    };
    this.immInitData = fromJS(this.data);
  }

  getShape(shapeName: string) {
    return this.data.shapes.find((shape: Shape) => shape.name === shapeName);
  }

  queueNewShape(text: string) {
    this.objectsCounter++;
    const newRect: Rectangle = {
      name: `object-${this.objectsCounter}`,
      type: ShapeType.rectangle,
      width: 100,
      height: 30,
      text,
      placement: [],
      selfPlacement: [],
    };
    this.newShapes.push(newRect);
  }

  async newShape(x: number, y: number) {
    this.isMovingShapeOutOfBoard = false;
    const newShape: Rectangle = this.newShapes[0] as Rectangle;
    newShape.x = x;
    newShape.y = y;
    return await this.graphCalcService.addShape(newShape);
  }

  refuseNewShapes() {
    this.newShapes = [];
  }

  queueNewLink(shapeName: string, type: string) {
    const newLink = {
      source: null,
      dest: null,
      translation: null,
      rotation: 0,
      length: 0,
      route: null,
    };
    newLink[type] = shapeName;
    this.newLinks.push(newLink);
  }

  async newLink(secondShapeName: string) {
    const newLink: Link = { ...this.newLinks[0] };
    let firstShapeName = newLink.source;
    let secondType = 'dest';
    if (!firstShapeName) {
      firstShapeName = newLink.dest;
      secondType = 'source';
    }
    newLink[secondType] = secondShapeName;
    return await this.graphCalcService.addLink(newLink);
  }

  refuseNewLinks() {
    this.newLinks = [];
  }

  toggleResizing(shapeName: string) {
    if (this.resizingShapeName === shapeName) {
      this.resizingShapeName = null;
    } else {
      this.resizingShapeName = shapeName;
    }
  }

  isShapeInResize(shapeName: string) {
    return this.resizingShapeName === shapeName;
  }

  removeShape(shapeName: string) {
    this.graphCalcService.removeShape(shapeName);
  }

  removeLink(source: string, dest: string) {
    this.graphCalcService.removeLink(source, dest);
  }

  async setShapePlacements(shape: Shape) {
    this.isMovingShapeOutOfBoard = false;
    const sendingShape = { ...shape };
    delete sendingShape.distinguished;
    return this.graphCalcService.setShapePlacements(sendingShape);
  }

  saveTitle(title: string): void {
    this.graphCalcService.saveTitle(title);
  }

  saveShapeText(shapeName: string, text: string): void {
    this.graphCalcService.saveShapeText(shapeName, text);
  }

  resetLinks() {
    for (const link of this.data.links) {
      link.translation = null;
      link.rotation = 0;
      link.length = 0;
      link.route = null;
    }
  }

  distinguishShape(shape: Shape) {
    this.data.shapes.map((aShape: Shape) => aShape.distinguished = false);
    this.data.links.map((aLink: Link) => aLink.distinguished = false);
    shape.distinguished = true;
  }

  distinguishLink(link: Link) {
    this.data.shapes.map((aShape: Shape) => aShape.distinguished = false);
    this.data.links.map((aLink: Link) => aLink.distinguished = false);
    link.distinguished = true;
  }

  extractGraphErrors(graphErrors: GraphError[]) {
    return this.extractGraphErrorsImpl('', graphErrors, []);
  }

  private extractGraphErrorsImpl(parentItems: string, graphErrors: GraphError[], messages: string[]) {
    let newMessages: string[] = [];
    for (const graphError of graphErrors) {
      if (graphError.children.length) {
        newMessages = newMessages.concat(
          this.extractGraphErrorsImpl(`${parentItems}${graphError.property} :: `, graphError.children, messages)
        );
      } else {
        for (const key in graphError.constraints) {
          newMessages.push(`${parentItems}${graphError.property} :: ${graphError.constraints[key]}`);
        }
        newMessages = messages.concat(newMessages);
      }
    }
    return newMessages;
  }
}

const RECT1: Rectangle = {
  name: 'object-100',
  type: ShapeType.rectangle,
  x: 50,
  y: 50,
  width: 200,
  height: 100,
  text: 'RECT1 - asdf asdf asdfasd asd fasd fasd fasd fasdf asdf asdf asdf asdf asdf asdf asdf asdf asdffa asdf asdf asdf...',
  placement: [],
  selfPlacement: [],
};
const RECT2: Rectangle = {
  name: 'object-101',
  type: ShapeType.rectangle,
  x: 93,
  y: 487,
  width: 200,
  height: 100,
  text: 'RECT2 - asdf asdf asdfasd asd fasd fasd fasd fasdf asdf asdf asdf asdf asdf asdf asdf asdf asdffa asdf asdf asdf...',
  placement: [],
  selfPlacement: [],
};

const RECT3: Rectangle = {
  name: 'object-102',
  type: ShapeType.rectangle,
  x: 40,
  y: 232,
  width: 178,
  height: 50,
  text: 'RECT3 - qwew qeqw erqwer qwe rqwerweq rqwr eqwerq...',
  placement: [],
  selfPlacement: [],
};

const RECT4: Rectangle = {
  name: 'object-103',
  type: ShapeType.rectangle,
  x: 42,
  y: 305,
  width: 56,
  height: 50,
  text: 'RECT4 - t rt r tr tr rt rt r tr tr rt rtrt rt...',
  placement: [],
  selfPlacement: [],
};

const RECT5: Rectangle = {
  name: 'object104',
  type: ShapeType.rectangle,
  x: 164,
  y: 384,
  width: 100,
  height: 50,
  text: 'RECT5 - nbmn bnmb b bn mb bn bnmbn mbn bnm bn mbnm bnm bnm...',
  placement: [],
  selfPlacement: [],
};

export const graphStubData = (): GraphData => {
  return {
    code: '2a',
    title: 'Test eate dn graph',
    width: GraphService.defaultWidth,
    height: GraphService.defaultHeight,
    shapes: [
      { ...RECT1 },
      { ...RECT2 },
      { ...RECT3 },
      { ...RECT4 },
      { ...RECT5 },
    ],
    links: [
      {
        source: RECT2.name,
        dest: RECT1.name,
        translation: {
          x: 228,
          y: 222
        },
        rotation: -137.29061004263855,
        length: 106.15083607772479,
        route: [
          [
            {
              x: 193,
              y: 487
            },
            {
              x: 154,
              y: 444
            }
          ],
          [
            {
              x: 154,
              y: 374
            },
            {
              x: 154,
              y: 444
            },
            {
              x: 164,
              y: 409
            },
            {
              x: 153,
              y: 409
            }
          ],
          [
            {
              x: 154,
              y: 374
            },
            {
              x: 228,
              y: 292
            }
          ],
          [
            {
              x: 228,
              y: 222
            },
            {
              x: 228,
              y: 292
            },
            {
              x: 218,
              y: 257
            },
            {
              x: 229,
              y: 257
            }
          ]
        ]
      },
      {
        source: RECT4.name,
        dest: RECT1.name,
        translation: {
          x: 30,
          y: 222
        },
        rotation: -30.96375653207352,
        length: 139.9428454762872,
        route: [
          [
            {
              x: 70,
              y: 305
            },
            {
              x: 30,
              y: 292
            }
          ],
          [
            {
              x: 30,
              y: 222
            },
            {
              x: 30,
              y: 292
            },
            {
              x: 40,
              y: 257
            },
            {
              x: 29,
              y: 257
            }
          ]
        ]
      },
      {
        source: RECT1.name,
        dest: RECT3.name,
        translation: {
          x: 150,
          y: 150
        },
        rotation: 104.36458244969721,
        length: 84.64632301523794,
        route: []
      },
    ]
  };
};

export const graphErrorStubData = (): GraphError[] => {
  return [
    {
      target: {},
      value: '',
      property: 'title',
      children: [],
      constraints: {
        length: 'title must be longer than or equal to 10 characters'
      }
    },
    {
      target: {},
      value: [],
      property: 'shapes',
      children: [
        {
          target: [],
          value: {},
          property: '0',
          children: [
            {
              target: {},
              value: 'sfas',
              property: 'text',
              children: [],
              constraints: {
                length: 'text must be longer than or equal to 10 characters'
              }
            }
          ]
        }
      ]
    }
  ];
};
