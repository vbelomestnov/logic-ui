import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { RestApiService } from './rest-api.service';
import { Observable, Subscriber } from 'rxjs';
import { LoginResponse } from '../types/login-response';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  get accessToken(): string {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem('auth');
    } else {
      return null;
    }
  }

  constructor(
    private restApiService: RestApiService,
    @Inject(PLATFORM_ID) private platformId: any
  ) { }

  login(login: string, password: string) {
    return new Observable((subscriber: Subscriber<void>) => {
      this.restApiService.login(login, password)
        .subscribe(
          (result: LoginResponse) => {
            localStorage.setItem('auth', result.access_token);
            subscriber.next();
          },
          (error) => {
            subscriber.error(error);
          }
        );
    });
  }

  drop() {
    localStorage.removeItem('auth');
  }
}
