import { TestBed } from '@angular/core/testing';

import { RestApiService } from './rest-api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_CONFIG } from '../app.config';

describe('RestApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [
      { provide: APP_CONFIG, useValue: {}},
    ]
  }));

  it('should be created', () => {
    const service: RestApiService = TestBed.get(RestApiService);
    expect(service).toBeTruthy();
  });
});
