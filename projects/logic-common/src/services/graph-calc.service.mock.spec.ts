import { Subject, of } from 'rxjs';
import { graphStubData } from './graph.service';

export class SocketTesting {

  public _exceptionMessage = null;
  public _graphSuccess = true;
  public _graphSubject: Subject<any> = new Subject<any>();
  public _exceptionSubject: Subject<any> = new Subject<any>();
  public _outOfBoardSubject: Subject<any> = new Subject<any>();
  public _stateSubject: Subject<any> = new Subject<any>();
  public _reconnectSubject: Subject<any> = new Subject<any>();

  ioSocket = {
    connected: true,
    disconnected: false,
  };

  _emitGraph(graph) {
    this._graphSubject.next({ success: this._graphSuccess, graph });
  }

  _emitException(exceptionMessage) {
    this._exceptionSubject.next(new Error(exceptionMessage));
  }

  _emitOutOfBoard(isOutOfBoard) {
    this._outOfBoardSubject.next(isOutOfBoard);
  }

  _emitState(state) {
    this._stateSubject.next(state);
  }

  _emitReconnect() {
    this._reconnectSubject.next();
  }

  emit(event, data) {
    return data;
  }

  fromEvent(event) {
    switch (event) {
      case 'graph':
        return this._graphSubject;
        break;
      case 'exception':
        return this._exceptionSubject;
        break;
      case 'outOfBoard':
        return this._outOfBoardSubject;
        break;
      case 'state':
        return this._stateSubject;
        break;
      case 'reconnect':
        return this._reconnectSubject;
        break;
      default:
        return of();
    }
  }

  fromOneTimeEvent(event) {
    switch (event) {
      case 'graph':
        return Promise.resolve({ success: this._graphSuccess, graph: graphStubData() });
        break;
      case 'exception':
        if (this._exceptionMessage) {
          return Promise.resolve(new Error(this._exceptionMessage));
        } else {
          return new Promise(() => { });
        }
        break;
      default:
        return Promise.resolve({});
    }
  }
}

export class SocketStub {

  ioSocket = {
    connected: true,
  };

  emit(event, data) { }

  fromEvent(event) {
    return of();
  }

  fromOneTimeEvent(event) {
    return Promise.resolve(null);
  }
}
