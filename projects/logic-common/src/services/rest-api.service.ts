import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GraphShortData, GraphServerData } from '../types/graph-data';
import { Observable } from 'rxjs';
import { AppConfig } from '../types/app-config';
import { APP_CONFIG } from '../app.config';
import { LoginResponse } from '../types/login-response';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  private readonly ROOT_URL: string;
  private readonly GRAPH_URL = '/graph';
  private readonly AUTH_URL = '/auth';
  private readonly LOGIN_URL = '/login';

  constructor(private http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
    this.ROOT_URL = appConfig.API_URL;
  }

  getGraphList(searchText: string, skip: number): Observable<GraphShortData[]> {
    return this.http.get<GraphShortData[]>(this.ROOT_URL + this.GRAPH_URL, { params: { search: searchText, skip: `${skip}` } });
  }

  getGraph(code: string): Observable<GraphServerData> {
    return this.http.get<GraphServerData>(this.ROOT_URL + this.GRAPH_URL + `/${code}`);
  }

  saveGraph(saveData: { socketId: string, svg: string }): Observable<GraphServerData> {
    return this.http.post<GraphServerData>(this.ROOT_URL + this.GRAPH_URL, saveData);
  }

  deleteGraph(code: string, authToken: string): Observable<boolean> {
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    return this.http.delete<boolean>(this.ROOT_URL + this.GRAPH_URL + `/${code}`, { headers });
  }

  reviewGraph(code: string, approved: boolean, authToken: string): Observable<boolean> {
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    return this.http.put<boolean>(this.ROOT_URL + this.GRAPH_URL + `/${code}/review`, { approved }, { headers });
  }

  login(login: string, password: string): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(this.ROOT_URL + this.AUTH_URL + this.LOGIN_URL, { login, password });
  }
}
