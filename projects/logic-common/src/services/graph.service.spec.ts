import { TestBed } from '@angular/core/testing';

import { GraphService, graphStubData, graphErrorStubData } from './graph.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_CONFIG } from '../app.config';
import { RestApiService } from './rest-api.service';
import { of, throwError } from 'rxjs';
import { GraphCalcService } from './graph-calc.service';
import { Socket } from 'ngx-socket-io';
import { SocketTesting } from './graph-calc.service.mock.spec';
import { GraphServerData } from 'logic-common/types/graph-data';
import { ShapeType } from '../types/enums/shape-type';
import { Rectangle } from '../types/shapes/rectangle';

describe('GraphService', () => {
  let service: GraphService;
  let restApiService: RestApiService;
  let socket: SocketTesting;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        RestApiService,
        GraphCalcService,
        { provide: Socket, useClass: SocketTesting },
      ]
    });

    service = TestBed.get(GraphService);
    restApiService = TestBed.get(RestApiService);
    socket = TestBed.get(Socket);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(service.isOutOfBoard).toBeFalsy();
    socket._emitOutOfBoard(true);
    expect(service.isOutOfBoard).toBeTruthy();
  });

  it('should create new graph', () => {
    const spy = spyOn(socket, 'emit');
    service.newGraph();
    expect(service.data.code).toBe('');
    expect(service.data.shapes.length).toBe(0);
    const args = spy.calls.mostRecent().args;
    expect(args).toEqual(['initGraph', 'new']);
  });

  it('should load a graph via rest api service', (done) => {
    const data = graphStubData();
    const spyGetGraph = spyOn(restApiService, 'getGraph').and.returnValue(of(data as GraphServerData));
    const spy = spyOn(socket, 'emit');
    service.loadGraph('2a').subscribe(
      (result) => {
        expect(spyGetGraph).toHaveBeenCalled();
        expect(result.code).toBe(data.code);
        const args = spy.calls.mostRecent().args;
        expect(args).toEqual(['initGraph', '2a']);

        spyGetGraph.and.returnValue(throwError('Error'));
        service.loadGraph('2a').subscribe(
          () => { },
          (error) => {
            expect(error).toBe('Error');
            done();
          }
        );
      }
    );
  });

  it('should add shape draft in queue while choosing position', () => {
    service.queueNewShape('New test shape');
    expect(service.newShapes.length).toBe(1);
    expect(service.newShapes[0] as Rectangle).toEqual({
      name: 'object-1',
      type: ShapeType.rectangle,
      width: 100,
      height: 30,
      text: 'New test shape',
      placement: [],
      selfPlacement: [],
    });
  });

  it('should save a graph via rest api service', (done) => {
    const data = graphStubData();
    service.data = data;
    const spySaveGraph = spyOn(restApiService, 'saveGraph').and.returnValue(of(data as GraphServerData));
    service.saveGraph('<svg></svg>').subscribe(
      (result) => {
        expect(spySaveGraph).toHaveBeenCalled();
        expect(result.code).toBe(data.code);
        spySaveGraph.and.returnValue(throwError('Error'));
        service.saveGraph('<svg></svg>').subscribe(
          () => { },
          (error) => {
            expect(error).toBe('Error');
            done();
          }
        );
      }
    );
  });

  it('should return a shape by its name', () => {
    const data = graphStubData();
    service.data = data;
    const shape = data.shapes[0];
    const foundShape = service.getShape('object-100');
    expect(shape.text).toBe(foundShape.text);
  });

  it('should clear newShapes on refuseNewShapes', () => {
    service.newGraph();
    service.queueNewShape('New Shape');
    expect(service.newShapes.length).toBe(1);
    service.refuseNewShapes();
    expect(service.newShapes.length).toBe(0);
  });

  it('should prevent creating new link if some conditions are not met', async () => {
    const data = graphStubData();
    service.data = data;
    const shape1Name = data.shapes[0].name;
    service.queueNewLink(shape1Name, 'source');
    try {
      socket._graphSuccess = false;
      socket._exceptionMessage = 'Source and destination are match';
      await service.newLink(shape1Name);
      fail('The new link creation have to fail');
    } catch (error) {
      expect(error.message).toBe('Source and destination are match');
    }

    try {
      const shape3Name = data.shapes[2].name;
      socket._graphSuccess = false;
      socket._exceptionMessage = 'There is duplicate link';
      await service.newLink(shape3Name);
      fail('The new link creation have to fail');
    } catch (error) {
      expect(error.message).toBe('There is duplicate link');
    }
  });

  it('should clear newLinks on refuseNewLinks', () => {
    service.newGraph();
    service.queueNewLink('object-1', 'dest');
    expect(service.newLinks.length).toBe(1);
    service.refuseNewLinks();
    expect(service.newLinks.length).toBe(0);
  });

  it('should check if shape in resize mode', () => {
    const data = graphStubData();
    service.data = data;
    expect(service.isShapeInResize('object-100')).toBe(false);
    service.toggleResizing('object-100');
    expect(service.isShapeInResize('object-100')).toBe(true);
  });

  it('should check if graph has been changed', async () => {
    service.newGraph();
    expect(service.hasChanged).toBe(false);
    service.queueNewShape('New shape');
    socket._emitGraph(graphStubData());
    expect(service.hasChanged).toBe(true);
  });

  it('should reset graph on socket reconnection', async () => {
    const spy = spyOn(socket, 'emit');
    socket._emitReconnect();
    expect(spy).not.toHaveBeenCalled();

    service.newGraph();
    spyOn(restApiService, 'getGraph').and.returnValue(of(service.data as GraphServerData));
    socket._emitReconnect();
    const args = spy.calls.mostRecent().args;
    expect(args).toEqual(['initGraph', 'new']);
  });

  it('should save title of graph', () => {
    const spy = spyOn(socket, 'emit');
    service.saveTitle('New custom title');
    const args = spy.calls.mostRecent().args;
    expect(args).toEqual(['saveTitle', 'New custom title']);
  });

  it('should save text of a shape via socket', () => {
    const text = 'New custom text';
    const shapeName = 'shape';
    const spy = spyOn(socket, 'emit');
    service.saveShapeText(shapeName, text);
    const args = spy.calls.mostRecent().args;
    expect(args).toEqual(['saveShapeText', { shapeName, text }]);
  });

  it('should update graph on "graph" socket event', () => {
    expect(service.data).toBeFalsy();
    socket._emitGraph(graphStubData());
    expect(service.data).toBeTruthy();
    service.queueNewLink(service.data.shapes[0].name, 'source');
    socket._graphSuccess = false;
    socket._emitGraph(graphStubData());
    expect(service.data).toBeTruthy();
    expect(service.newLinks.length).toBe(1);
  });

  it('sould request for new link creation', () => {
    socket._emitGraph(graphStubData());
    socket._exceptionMessage = null;
    socket._graphSuccess = true;
    service.queueNewLink(service.data.shapes[0].name, 'dest');
    service.newLink(service.data.shapes[0].name);
    socket._emitGraph(graphStubData());
    expect(service.newLinks.length).toBe(0);
  });

  it('sould remove shape and remove link via socket', () => {
    socket._emitGraph(graphStubData());
    const spy = spyOn(socket, 'emit');
    service.removeLink(service.data.links[0].source, service.data.links[0].dest);
    expect(spy).toHaveBeenCalledTimes(1);
    let args = spy.calls.mostRecent().args;
    expect(args).toEqual(['removeLink', { source: service.data.links[0].source, dest: service.data.links[0].dest }]);

    service.removeShape(service.data.shapes[0].name);
    expect(spy).toHaveBeenCalledTimes(2);
    args = spy.calls.mostRecent().args;
    expect(args).toEqual(['removeShape', service.data.shapes[0].name]);
  });

  it('sould extract graphErrors', () => {
    expect(service.extractGraphErrors(graphErrorStubData())).toEqual([
      'title :: title must be longer than or equal to 10 characters',
      'shapes :: 0 :: text :: text must be longer than or equal to 10 characters',
    ]);
  });
});
