import { TestBed } from '@angular/core/testing';

import { GraphCalcService } from './graph-calc.service';
import { Socket } from 'ngx-socket-io';
import { SocketTesting } from './graph-calc.service.mock.spec';

describe('GraphCalcService', () => {
  let service: GraphCalcService;
  let socket: SocketTesting;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Socket, useClass: SocketTesting },
      ]
    });
    service = TestBed.get(GraphCalcService);
    socket = TestBed.get(Socket);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should listen to "state" event', () => {
    expect(service.state).toBe('empty');
    socket._emitState('test_state');
    expect(service.state).toBe('test_state');
  });

  it('should listen to "exception" event', () => {
    const spy = spyOn(console, 'error');
    socket._emitException('test exception');
    expect(spy).toHaveBeenCalled();
  });

  it('should init graph via socket', () => {
    const spy = spyOn(socket, 'emit');
    service.initGraph('aaa');
    expect(spy).toHaveBeenCalled();
    const args = spy.calls.mostRecent().args;
    expect(args).toEqual(['initGraph', 'aaa']);
  });

  it('should break calculations', () => {
    const spy = spyOn(socket, 'emit');
    service.breakCalculations();
    expect(spy).toHaveBeenCalled();
    const args = spy.calls.mostRecent().args;
    expect(args).toEqual(['break']);
  });
});
