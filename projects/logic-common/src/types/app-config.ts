export interface AppConfig {
  production: boolean;
  API_URL: string;
  SOCKET_URL: string;
  STATIC_URL: string;
}
