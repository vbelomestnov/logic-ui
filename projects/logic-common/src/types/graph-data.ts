import { Link, ServerLink } from './shapes/link';
import { Shape, ServerShape } from './shapes/shape';

export interface GraphShortData {
  code: string;
  title: string;
}

export interface GraphServerData extends GraphShortData {
  _id: string;
  __v: number;
  width: number;
  height: number;
  shapes: ServerShape[];
  links: ServerLink[];
  approved?: boolean;
  archived?: boolean;
}


export interface GraphData extends GraphShortData {
  width: number;
  height: number;
  shapes: Shape[];
  links: Link[];
  approved?: boolean;
  archived?: boolean;
}
