export enum GraphMode {
    resize,
    preview,
    disabled,
    readonly,
}
