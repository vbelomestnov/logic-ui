export interface GraphError {
  children: GraphError[];
  constraints?: {
    [key: string]: string;
  };
  property: string;
  target: any;
  value: any;
}
