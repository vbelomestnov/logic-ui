import { Point } from './point';

export interface OriginLink {
    source: string;
    dest: string;
    translation: Point;
    rotation: number;
    length: number;
    route: Point[][];
}

export interface Link extends OriginLink {
    distinguished?: boolean;
}

export interface ServerLink extends OriginLink {
    _id: any;
}
