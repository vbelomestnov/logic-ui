import { Point } from './point';
import { DimensionLine } from './dimension-line';
import { ShapeType } from '../enums/shape-type';

export interface Shape extends Point {
    name: string;
    type: ShapeType;
    text: string;
    placement: DimensionLine[];
    selfPlacement: DimensionLine[];
    isCovered?: boolean;
    distinguished?: boolean;
}

export interface ServerShape extends Shape {
    _id: any;
}
