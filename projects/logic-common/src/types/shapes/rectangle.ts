import { Shape } from './shape';
import { Size } from './size';

export interface Rectangle extends Shape, Size {}
