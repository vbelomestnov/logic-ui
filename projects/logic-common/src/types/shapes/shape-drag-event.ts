import { DragType } from '../enums/drag-type';

export interface ShapeDragEvent {
    shapeName: string;
    dragType: DragType;
    x: number;
    y: number;
}
