import { Point } from './point';
import { LineType } from '../enums/line-type';
import { Side } from '../enums/side';

export interface DimensionLine {
    type: LineType;
    side: Side;
    points: Point[];
}
