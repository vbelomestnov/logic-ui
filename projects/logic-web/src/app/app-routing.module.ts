import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestGraphComponent } from './components/test-graph/test-graph.component';
import { SearchComponent } from './components/search/search.component';
import { GraphPageComponent } from './components/graph-page/graph-page.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';

const appRoutes: Routes = [
  { path: 'search', component: SearchComponent },
  { path: 'graph/:code', component: GraphPageComponent },
  { path: 'test-view', component: TestGraphComponent },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: '/search', pathMatch: 'full' },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
