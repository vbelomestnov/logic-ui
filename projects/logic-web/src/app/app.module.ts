import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Material
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDividerModule} from '@angular/material/divider';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {PortalModule} from '@angular/cdk/portal';
import {MatCardModule} from '@angular/material/card';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { GraphModule, APP_CONFIG } from 'logic-common';
import { HttpClientModule } from '@angular/common/http';
import { RestApiService } from 'logic-common';
import { GraphService } from 'logic-common';
import { environment } from 'environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { TestGraphComponent } from './components/test-graph/test-graph.component';
import { SearchComponent } from './components/search/search.component';
import { GraphPageComponent } from './components/graph-page/graph-page.component';
import { RemoveDialogComponent } from './components/remove-dialog/remove-dialog.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { LoadingComponent } from './service/helper.service';
import { GraphErrorSnackbarComponent } from './components/graph-error-snackbar/graph-error-snackbar.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';

const config: SocketIoConfig = { url: environment.SOCKET_URL, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    TestGraphComponent,
    SearchComponent,
    GraphPageComponent,
    RemoveDialogComponent,
    LoadingComponent,
    GraphErrorSnackbarComponent,
    LoginComponent,
    AdminComponent,
  ],
  entryComponents: [
    RemoveDialogComponent,
    LoadingComponent,
    GraphErrorSnackbarComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    SocketIoModule.forRoot(config),
    BrowserAnimationsModule,
    GraphModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    // Material
    MatToolbarModule,
    MatListModule,
    MatInputModule,
    MatIconModule,
    MatSnackBarModule,
    ScrollingModule,
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    MatTooltipModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    PortalModule,
    MatCardModule,
    MatSlideToggleModule,
  ],
  providers: [
    RestApiService,
    GraphService,
    { provide: APP_CONFIG, useValue: environment},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
