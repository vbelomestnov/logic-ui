export enum EditViewMode {
    root,
    shapeEdit,
    shapeNew,
    newPosition,
    newLink,
    preview,
}
