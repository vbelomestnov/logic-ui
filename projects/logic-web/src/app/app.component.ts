import { Component, OnInit } from '@angular/core';
import { AuthService } from 'logic-common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  constructor(
    public authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit() { }

  logout() {
    this.authService.drop();
    this.router.navigate(['login']);
  }
}
