import { TestBed } from '@angular/core/testing';

import { HelperService } from './helper.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Overlay } from '@angular/cdk/overlay';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_CONFIG, SocketTesting } from 'logic-common';
import { Socket } from 'ngx-socket-io';

describe('HelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule,],
    providers: [
      MatSnackBar, Overlay,
      { provide: APP_CONFIG, useValue: {} },
      { provide: Socket, useClass: SocketTesting },
    ]
  }));

  it('should be created', () => {
    const service: HelperService = TestBed.get(HelperService);
    expect(service).toBeTruthy();
  });
});
