import { Injectable, Component, ViewContainerRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OverlayRef, OverlayConfig, Overlay } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { GraphService } from 'logic-common';
import { GraphErrorSnackbarComponent } from '../components/graph-error-snackbar/graph-error-snackbar.component';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  private overlayRef: OverlayRef = null;
  private portal: ComponentPortal<LoadingComponent> = null;

  constructor(
    private snackBar: MatSnackBar,
    private graphService: GraphService,
    private overlay?: Overlay,
  ) { }

  openErrorSnackBar(message: string) {
    let messages: string[] = [];
    if (typeof message === 'string') {
      messages.push(message);
    } else {
      messages = this.graphService.extractGraphErrors(message);
    }
    this.snackBar.openFromComponent(GraphErrorSnackbarComponent, {
      data: messages,
      duration: 20000,
      panelClass: 'error-snack-bar',
    });
  }

  public showLoading(viewContainerRef: ViewContainerRef) {
    if (this.overlayRef) {
      return;
    }
    const config = new OverlayConfig();

    config.positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    config.hasBackdrop = true;

    this.overlayRef = this.overlay.create(config);
    this.portal = new ComponentPortal(LoadingComponent, viewContainerRef);
    this.overlayRef.attach(this.portal);
  }

  public hideLoading() {
    setTimeout(() => {
      if (this.overlayRef) {
        this.overlayRef.dispose();
        this.overlayRef = null;
      }
    }, 300);
  }

}

@Component({
  selector: 'app-loading',
  template: '<mat-spinner></mat-spinner>'
})
export class LoadingComponent { }
