import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphErrorSnackbarComponent } from './graph-error-snackbar.component';
import { MatSnackBarModule, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

describe('GraphErrorSnackbarComponent', () => {
  let component: GraphErrorSnackbarComponent;
  let fixture: ComponentFixture<GraphErrorSnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GraphErrorSnackbarComponent],
      imports: [MatSnackBarModule,],
      providers: [
        {provide: MAT_SNACK_BAR_DATA, useValue: ['1', '2']}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphErrorSnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
