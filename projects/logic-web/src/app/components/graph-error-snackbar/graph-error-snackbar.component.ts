import { Component, OnInit, Inject } from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-graph-error-snackbar',
  templateUrl: './graph-error-snackbar.component.html',
  styleUrls: ['./graph-error-snackbar.component.scss'],
})
export class GraphErrorSnackbarComponent implements OnInit {

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: string[],
    public snackBar: MatSnackBar,
    ) { }

  ngOnInit() {}

}
