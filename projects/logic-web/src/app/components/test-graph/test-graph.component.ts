import { Component, OnInit, ViewChild } from '@angular/core';
import { GraphService, graphStubData, GraphData, GraphMode, GraphComponent } from 'logic-common';

@Component({
  selector: 'app-test-graph',
  templateUrl: './test-graph.component.html',
  styleUrls: ['./test-graph.component.scss'],
  providers: [
    GraphService,
  ]
})
export class TestGraphComponent implements OnInit {

  x: number = null;
  y: number = null;

  GraphMode = GraphMode;
  data: GraphData = null;

  @ViewChild(GraphComponent, {static: false}) graph: GraphComponent;

  constructor(public graphService: GraphService) {
    graphService.data = graphStubData();
  }

  ngOnInit() {
    this.data = this.graphService.data;
  }

  clickHandler(event: MouseEvent) {
    const rect = this.graph.rectElement.nativeElement.getBoundingClientRect();
    const widthRatio = this.data.width / rect.width;
    const heightRatio = this.data.height / rect.height;
    this.x = (event.clientX - rect.x) * widthRatio;
    this.y = (event.clientY - rect.y) * heightRatio;
  }

}
