import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_CONFIG, GraphModule, SocketTesting } from 'logic-common';
import { TestGraphComponent } from './test-graph.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Socket } from 'ngx-socket-io';

describe('TestGraphComponent', () => {
  let component: TestGraphComponent;
  let fixture: ComponentFixture<TestGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestGraphComponent],
      imports: [HttpClientTestingModule, NoopAnimationsModule, GraphModule],
      providers: [
        { provide: APP_CONFIG, useValue: {}},
        { provide: Socket, useClass: SocketTesting },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show coordinates on click on svg', () => {
    component.clickHandler(new MouseEvent('click', { clientX: 240, clientY: 435 }));
    expect(component.x).toBeTruthy();
    expect(component.y).toBeTruthy();
    expect(component.x).toBeLessThan(component.y);
  });
});
