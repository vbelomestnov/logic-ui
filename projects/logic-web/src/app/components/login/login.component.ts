import { Component, OnInit } from '@angular/core';
import { AuthService } from 'logic-common';
import { Router } from '@angular/router';
import { HelperService } from '../../service/helper.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  login = '';
  password = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private helperService: HelperService,
  ) { }

  ngOnInit() {}

  handleLogin() {
    this.authService.login(this.login, this.password).subscribe(
      () => {
        this.router.navigate(['admin']);
      },
      (error) => {
        this.helperService.openErrorSnackBar(error.message);
      }
    );
  }
}
