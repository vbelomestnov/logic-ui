import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { GraphService, GraphShortData, GraphData, RestApiService, AuthService } from 'logic-common';
import { HelperService } from '../../service/helper.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSlideToggleChange, MatSlideToggle } from '@angular/material/slide-toggle';
import { MatDialog } from '@angular/material/dialog';
import { RemoveDialogComponent } from '../remove-dialog/remove-dialog.component';
import { SearchComponent } from '../search/search.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {

  @ViewChild('approveSlide', { static: false, read: MatSlideToggle }) approveSlideElement: MatSlideToggle;
  @ViewChild('search', {static: false, read: SearchComponent}) searchComponent: SearchComponent;

  constructor(
    public graphService: GraphService,
    private helperService: HelperService,
    private viewContainerRef: ViewContainerRef,
    private restService: RestApiService,
    private authService: AuthService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() { }

  selectGraph(graph: GraphShortData) {
    this.loadData(graph.code);
  }

  loadData(code) {
    this.helperService.showLoading(this.viewContainerRef);
    this.graphService.loadGraph(code)
      .subscribe(
        (graph: GraphData) => {
          this.helperService.hideLoading();
        },
        (error: HttpErrorResponse) => {
          let message = error.statusText;
          if (error.error && error.error.message) {
            message = error.error.message;
          }
          this.helperService.openErrorSnackBar(message);
          this.helperService.hideLoading();
        }
      );
  }

  approvedChange(event: MatSlideToggleChange, code: string) {
    this.helperService.showLoading(this.viewContainerRef);
    this.restService.reviewGraph(code, event.checked, this.authService.accessToken)
      .subscribe((result) => {
        this.helperService.hideLoading();
        this.searchComponent.searchChange('');
      }, (error) => {
        this.helperService.hideLoading();
        this.approveSlideElement.toggle();
        this.helperService.openErrorSnackBar(error.message);
      });
  }

  delete(code: string) {
    const dialogRef = this.dialog.open(RemoveDialogComponent, {
      width: '250px',
      data: { title: 'Link removal' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.helperService.showLoading(this.viewContainerRef);
        this.restService.deleteGraph(code, this.authService.accessToken)
          .subscribe((result) => {
            this.helperService.hideLoading();
            this.searchComponent.searchChange('');
          }, (error) => {
            this.helperService.hideLoading();
            this.helperService.openErrorSnackBar(error.message);
          });
      }
    });
  }
}
