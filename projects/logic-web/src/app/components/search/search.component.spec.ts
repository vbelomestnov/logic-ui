import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_CONFIG, RestApiService, GraphShortData, SocketTesting } from 'logic-common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { ScrollingModule, CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { Socket } from 'ngx-socket-io';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { LoadingComponent } from '../../service/helper.service';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let router: Router;
  let restApiService: RestApiService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent, LoadingComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, RouterTestingModule, ScrollingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        MatSnackBar, Overlay,
        { provide: Socket, useClass: SocketTesting },
      ],
    })
    .overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [
          LoadingComponent,
        ],
      }
    })
      .compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    restApiService = TestBed.get(RestApiService);

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update ds filter on filter field change', fakeAsync(() => {
    const spyFilter = spyOnProperty(component.graphListDataSource, 'filter', 'set');
    component.searchFormControl.setValue(null);
    expect(spyFilter).not.toHaveBeenCalled();
    tick(900);
    expect(spyFilter).toHaveBeenCalled();
    let arg = spyFilter.calls.mostRecent().args[0];
    expect(arg).toBe('');
    spyFilter.calls.reset();

    component.searchFormControl.setValue('asdf');
    tick(900);
    expect(spyFilter).toHaveBeenCalled();
    arg = spyFilter.calls.mostRecent().args[0];
    expect(arg).toBe('asdf');
    spyFilter.calls.reset();
  }));

  it('should navigate graph/new on add new graph', fakeAsync(() => {
    const spyNavigate = spyOn(router, 'navigate');
    const buttons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    buttons[0].nativeElement.click();
    expect(spyNavigate).toHaveBeenCalled();
  }));

  it('should load data on scroll down (infinite scroll)', (done) => {
    let viewportEl = fixture.debugElement.query(By.directive(CdkVirtualScrollViewport));
    let viewportComponent = viewportEl.injector.get(CdkVirtualScrollViewport);
    const spyLoadList = spyOn(restApiService, 'getGraphList').and.returnValue(of(stubList()));
    component.searchFormControl.setValue('asdf');
    const firstSubscription = viewportComponent.renderedRangeStream.subscribe(
      () => {
        fixture.detectChanges();
        firstSubscription.unsubscribe();
        expect(spyLoadList).toHaveBeenCalledTimes(1);
        doSecondSubscription();
      }
    );
    const doSecondSubscription = () => {
      viewportEl = fixture.debugElement.query(By.directive(CdkVirtualScrollViewport));
      viewportComponent = viewportEl.injector.get(CdkVirtualScrollViewport);
      viewportComponent.scrollToIndex(15);
      const secondSubscription = viewportComponent.renderedRangeStream.subscribe(
        () => {
          fixture.detectChanges();
          secondSubscription.unsubscribe();
          expect(spyLoadList).toHaveBeenCalledTimes(1);
          doThirdSubscription();
        }
      );
    };

    const doThirdSubscription = () => {
      viewportEl = fixture.debugElement.query(By.directive(CdkVirtualScrollViewport));
      viewportComponent = viewportEl.injector.get(CdkVirtualScrollViewport);
      viewportComponent.scrollToIndex(50);
      const secondSubscription = viewportComponent.renderedRangeStream.subscribe(
        () => {
          fixture.detectChanges();
          secondSubscription.unsubscribe();
          expect(spyLoadList).toHaveBeenCalledTimes(2);
          done();
        }
      );
    };
  });
});

const stubList = (): GraphShortData[] => {
  const result: GraphShortData[] = [];
  for (let i = 1; i <= 50; i++) {
    result.push({ code: `a${i}`, title: `Graph ${i}` });
  }
  return result;
};
