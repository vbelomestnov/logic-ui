import { GraphListDataSource } from './graph-list-data-source';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RestApiService, APP_CONFIG, SocketTesting } from 'logic-common';
import { HelperService, LoadingComponent } from '../../service/helper.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Overlay } from '@angular/cdk/overlay';
import { of, throwError } from 'rxjs';
import { ViewContainerRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SearchComponent } from './search.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { Socket } from 'ngx-socket-io';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('GraphListDataSource', () => {
  let restApiService: RestApiService;
  let helperService: HelperService;
  let viewContainerRef: ViewContainerRef;
  let fixture: ComponentFixture<SearchComponent>;
  let component: SearchComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent, LoadingComponent],
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        FormsModule,
        RouterTestingModule,
        ScrollingModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatInputModule,
        NoopAnimationsModule, 
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        MatSnackBar, Overlay, ViewContainerRef,
        { provide: Socket, useClass: SocketTesting },
      ]
    })
      .overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [
            LoadingComponent,
          ],
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    restApiService = TestBed.get(RestApiService);
    helperService = TestBed.get(HelperService);
    viewContainerRef = TestBed.get(ViewContainerRef);

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create an instance', () => {
    expect(new GraphListDataSource(restApiService, helperService, viewContainerRef)).toBeTruthy();
  });

  it('should load data on init', () => {
    const spyLoadList = spyOn(restApiService, 'getGraphList').and.returnValue(of([{ code: 'aab', title: 'First Graph' }]));
    const ds = new GraphListDataSource(restApiService, helperService, viewContainerRef);
    expect(spyLoadList).toHaveBeenCalled();
  });

  it('should call load data on filter change', () => {
    const ds = new GraphListDataSource(restApiService, helperService, viewContainerRef);
    const spyLoad = spyOn(ds, 'loadData');
    ds.filter = 'asdf';
    expect(spyLoad).toHaveBeenCalled();
    ds.filter = 'asdf';
    expect(spyLoad).toHaveBeenCalledTimes(1);
    ds.filter = 'asde';
    expect(spyLoad).toHaveBeenCalledTimes(2);
    const filterValue = ds.filter;
    expect(filterValue).toBe('asde');
  });

  it('should handle load error properly', () => {
    const spySnackBar = spyOn(helperService, 'openErrorSnackBar');
    const errorMessageStatusText = 'DB ERROR status text';
    const spyLoadList = spyOn(restApiService, 'getGraphList').and.returnValue(throwError({ statusText: errorMessageStatusText }));
    let ds = new GraphListDataSource(restApiService, helperService, viewContainerRef);
    let messageArg = spySnackBar.calls.mostRecent().args[0];
    expect(messageArg).toBe(errorMessageStatusText);

    const errorMessageMessage = 'DB ERROR message';
    spyLoadList.and.returnValue(throwError({ error: { message: errorMessageMessage } }));
    ds = new GraphListDataSource(restApiService, helperService, viewContainerRef);
    messageArg = spySnackBar.calls.mostRecent().args[0];
    expect(messageArg).toBe(errorMessageMessage);
  });
});
