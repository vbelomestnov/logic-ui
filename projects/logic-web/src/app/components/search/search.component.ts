import { Component, OnInit, ViewContainerRef, Input, Output, EventEmitter } from '@angular/core';
import { RestApiService, GraphShortData } from 'logic-common';
import { FormControl } from '@angular/forms';
import { debounce } from 'rxjs/operators';
import { interval } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { GraphListDataSource } from './graph-list-data-source';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {

  @Input() preventFollowGraph;
  @Output() selectGraph: EventEmitter<GraphShortData> = new EventEmitter<GraphShortData>();

  public searchFormControl: FormControl;
  public graphListDataSource: GraphListDataSource;

  constructor(
    private restApiService: RestApiService,
    private helperService: HelperService,
    private router: Router,
    private viewContainerRef: ViewContainerRef,
  ) {
    this.searchFormControl = new FormControl('', []);
    this.searchFormControl.valueChanges
      .pipe(
        debounce(() => interval(800))
      )
      .subscribe((newValue) => {
        this.searchChange(newValue == null ? '' : newValue);
      });
  }

  ngOnInit() {
    this.graphListDataSource = new GraphListDataSource(this.restApiService, this.helperService, this.viewContainerRef);
  }

  searchChange(value: string) {
    this.graphListDataSource.filter = value;
  }

  newGraph() {
    this.router.navigate(['/graph', 'new']);
  }

  selectGraphHandler(graph: GraphShortData) {
    this.selectGraph.emit(graph);
  }

}
