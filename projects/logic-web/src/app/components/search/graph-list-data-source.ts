import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { GraphShortData, RestApiService } from 'logic-common';
import { BehaviorSubject, Subscription, Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { HelperService } from '../../service/helper.service';
import { ViewContainerRef } from '@angular/core';

export class GraphListDataSource extends DataSource<GraphShortData | undefined> {
  private cachedFacts: GraphShortData[] = [];
  private dataStream = new BehaviorSubject<(GraphShortData | undefined)[]>(this.cachedFacts);
  private subscription = new Subscription();
  private offset = 0;

  private _filter = '';
  get filter(): string {
    return this._filter;
  }
  set filter(value: string) {
    if (value === this._filter) {
      return;
    }
    this._filter = value;
    this.offset = 0;
    this.cachedFacts = [];
    this.loadData();
  }

  constructor(
    private restApiService: RestApiService,
    private helperService: HelperService,
    private viewContainerRef: ViewContainerRef,
  ) {
    super();
    this.loadData();
  }

  connect(collectionViewer: CollectionViewer): Observable<(GraphShortData | undefined)[] | ReadonlyArray<GraphShortData | undefined>> {
    this.subscription.add(collectionViewer.viewChange.subscribe(range => {
      if (range.end === this.offset) {
        this.loadData();
      }
    }));
    return this.dataStream;
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.subscription.unsubscribe();
  }

  loadData(): void {
    this.helperService.showLoading(this.viewContainerRef);
    this.restApiService.getGraphList(this._filter, this.offset)
      .subscribe(
        (result: GraphShortData[]) => {
          this.offset += result.length;
          this.cachedFacts = this.cachedFacts.concat(result);
          this.dataStream.next(this.cachedFacts);
          this.helperService.hideLoading();
        },
        (error: HttpErrorResponse) => {
          let message = error.statusText;
          if (error.error && error.error.message) {
            message = error.error.message;
          }
          this.helperService.openErrorSnackBar(message);
          this.helperService.hideLoading();
        }
      );
  }
}
