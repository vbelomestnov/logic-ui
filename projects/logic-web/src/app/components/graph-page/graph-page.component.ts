import { Component, OnInit, ViewChild, NgZone, ViewContainerRef } from '@angular/core';
import { GraphService, GraphData, GraphMode, Shape, Link, GraphCalcService, GraphComponent } from 'logic-common';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { HelperService } from '../../service/helper.service';
import { MatMenuTrigger } from '@angular/material/menu';
import { EditViewMode } from '../../types/edit-view-mode';
import { MatDialog } from '@angular/material/dialog';
import { RemoveDialogComponent } from '../remove-dialog/remove-dialog.component';
import { Socket } from 'ngx-socket-io';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-graph-page',
  templateUrl: './graph-page.component.html',
  styleUrls: ['./graph-page.component.scss'],
})
export class GraphPageComponent implements OnInit {
  GraphMode = GraphMode;
  EditViewMode = EditViewMode;

  public graphMode: GraphMode = GraphMode.preview;
  public shapeSelected: Shape = null;
  public shapeText: string = null;
  public showNewPositionControls = false;
  public showNewLinkControls = false;
  public showEditorRootControls = false;
  public titleEdited: string = null;

  public linkSelected: Link = null;
  public code: string = null;

  get displayedCode(): string {
    if (this.graphService.data && this.graphService.data.code) {
      return this.graphService.data.code;
    } else if (this.code) {
      return this.code;
    } else {
      return '[new graph]';
    }
  }

  @ViewChild('shapeMenuTrigger', { static: false, read: MatMenuTrigger }) private shapeMenuTrigger: MatMenuTrigger;
  @ViewChild('linkMenuTrigger', { static: false, read: MatMenuTrigger }) private linkMenuTrigger: MatMenuTrigger;
  @ViewChild('graph', { static: false, read: GraphComponent }) graphElement: GraphComponent;
  public menuTriggerStyle = {
    top: '0px',
    left: '0px',
  };

  constructor(
    public graphService: GraphService,
    private route: ActivatedRoute,
    private helperService: HelperService,
    public dialog: MatDialog,
    private router: Router,
    public socket: Socket,
    public calcService: GraphCalcService,
    private viewContainerRef: ViewContainerRef,
    private meta: Meta,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        const code = params.code;
        if (code === 'new') {
          this.graphService.newGraph();
          this.setMode(EditViewMode.root);
        } else {
          this.code = code;
          this.loadData();
        }
      });
    this.calcService.listenException()
      .subscribe((error: Error) => {
        this.helperService.openErrorSnackBar(error.message);
      });
  }

  loadData() {
    this.helperService.showLoading(this.viewContainerRef);
    this.graphService.loadGraph(this.code)
      .subscribe(
        (graph: GraphData) => {
          this.meta.addTags([
            { name: 'twitter:title', content: `Code: ${graph.code}. ${graph.title}` },
            { name: 'twitter:image', content: `/png/${graph.code}.png` },
          ]);
          this.setMode(EditViewMode.preview);
          this.helperService.hideLoading();
        },
        (error: HttpErrorResponse) => {
          let message = error.statusText;
          if (error.error && error.error.message) {
            message = error.error.message;
          }
          this.helperService.openErrorSnackBar(message);
          this.helperService.hideLoading();
        }
      );
  }

  setMode(newMode: EditViewMode) {
    switch (newMode) {
      case EditViewMode.root:
        this.shapeSelected = null;
        this.shapeText = null;
        this.showNewPositionControls = false;
        this.showNewLinkControls = false;
        this.showEditorRootControls = true;
        this.graphMode = null;
        break;
      case EditViewMode.shapeEdit:
        this.showEditorRootControls = false;
        this.shapeText = this.shapeSelected.text;
        this.graphMode = GraphMode.readonly;
        this.titleEdited = null;
        break;
      case EditViewMode.shapeNew:
        this.showEditorRootControls = false;
        this.shapeText = '';
        this.shapeSelected = null;
        this.graphMode = GraphMode.readonly;
        this.titleEdited = null;
        break;
      case EditViewMode.newPosition:
        this.shapeText = null;
        this.showNewPositionControls = true;
        this.graphMode = GraphMode.readonly;
        this.titleEdited = null;
        break;
      case EditViewMode.newLink:
        this.shapeText = null;
        this.showNewLinkControls = true;
        this.showEditorRootControls = false;
        this.graphMode = GraphMode.readonly;
        this.titleEdited = null;
        break;
      case EditViewMode.preview:
        this.showEditorRootControls = false;
        this.graphMode = GraphMode.preview;
        this.titleEdited = null;
        break;
    }
  }

  lock() {
    if (!this.graphService.hasChanged) {
      this.loadData();
    } else {
      const dialogRef = this.dialog.open(RemoveDialogComponent, {
        width: '250px',
        data: { title: 'Discard data' }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.loadData();
        }
      });
    }
  }

  rejectEditActions() {
    this.graphService.refuseNewShapes();
    this.graphService.refuseNewLinks();
    this.setMode(EditViewMode.root);
  }

  saveShape() {
    if (!this.shapeSelected) {
      this.graphService.queueNewShape(this.shapeText);
      this.setMode(EditViewMode.newPosition);
    } else {
      this.graphService.saveShapeText(this.shapeSelected.name, this.shapeText);
      this.setMode(EditViewMode.root);
    }
  }

  createLink(type) {
    this.graphService.queueNewLink(this.shapeSelected.name, type);
    this.setMode(EditViewMode.newLink);
  }

  removeShape() {
    const dialogRef = this.dialog.open(RemoveDialogComponent, {
      width: '250px',
      data: { title: 'Shape removal' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.graphService.removeShape(this.shapeSelected.name);
      }
    });
  }

  removeLink() {
    const dialogRef = this.dialog.open(RemoveDialogComponent, {
      width: '250px',
      data: { title: 'Link removal' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.graphService.removeLink(this.linkSelected.source, this.linkSelected.dest);
      }
    });
  }

  save() {
    this.helperService.showLoading(this.viewContainerRef);
    this.graphService.saveGraph(this.graphElement.svgElement.nativeElement.outerHTML)
      .subscribe(
        (result: GraphData) => {
          this.router.navigate(['/graph', result.code]);
          this.helperService.hideLoading();
        },
        (error: HttpErrorResponse) => {
          let message = error.statusText;
          if (error.error && error.error.message) {
            message = error.error.message;
          } else if (error.error && typeof error.error.error === 'string') {
            message = error.error.error;
          }
          this.helperService.openErrorSnackBar(message);
          this.helperService.hideLoading();
        }
      );
  }

  editTitle() {
    this.titleEdited = this.graphService.data.title;
  }

  saveTitle() {
    this.graphService.saveTitle(this.titleEdited);
    this.titleEdited = null;
  }

  undoTitle() {
    this.titleEdited = null;
  }

  shapeClick({ shape, event }) {
    if (!this.graphMode) {
      this.shapeSelected = shape;
      this.menuTriggerStyle.top = `${event.clientY}px`;
      this.menuTriggerStyle.left = `${event.clientX}px`;
      this.shapeMenuTrigger.openMenu();
    } else if (this.graphService.newLinks.length && this.graphMode === GraphMode.readonly) {
      this.graphService.newLink(shape.name)
        .then(() => {
          this.setMode(EditViewMode.root);
        })
        .catch(error => {
          // this.helperService.openErrorSnackBar(error.message);
        });
    }
  }

  linkClick({ link, event }) {
    if (!this.graphMode) {
      this.linkSelected = link;
      this.menuTriggerStyle.top = `${event.clientY}px`;
      this.menuTriggerStyle.left = `${event.clientX}px`;
      this.linkMenuTrigger.openMenu();
    }
  }

  shapeCreated(newShape: Shape) {
    this.setMode(EditViewMode.root);
  }

}
