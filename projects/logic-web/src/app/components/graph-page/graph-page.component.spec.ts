import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { GraphPageComponent } from './graph-page.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  APP_CONFIG,
  GraphService,
  graphStubData,
  GraphModule,
  RectTextComponent,
  LinkComponent,
  GraphMode,
  SocketTesting,
  graphErrorStubData,
} from 'logic-common';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule, MatMenuTrigger } from '@angular/material/menu';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { of, throwError } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { RemoveDialogComponent } from '../remove-dialog/remove-dialog.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HelperService, LoadingComponent } from '../../service/helper.service';
import { By } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { EditViewMode } from '../../types/edit-view-mode';
import { Socket } from 'ngx-socket-io';
import { GraphErrorSnackbarComponent } from '../graph-error-snackbar/graph-error-snackbar.component';

describe('GraphPageComponent', () => {
  let component: GraphPageComponent;
  let fixture: ComponentFixture<GraphPageComponent>;
  let route: ActivatedRoute;
  let graphService: GraphService;
  let helperService: HelperService;
  let dialog: MatDialog;
  let socket: SocketTesting;
  let router: Router;
  let snackBar: MatSnackBar;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GraphPageComponent, RemoveDialogComponent, LoadingComponent, GraphErrorSnackbarComponent,],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([{ path: 'graph/:code', component: GraphPageComponent }]),
        MatMenuModule,
        FormsModule,
        MatDialogModule,
        MatSnackBarModule,
        NoopAnimationsModule,
        MatButtonModule,
        GraphModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: APP_CONFIG, useValue: {} },
        { provide: ActivatedRoute, useClass: ActivatedRouteTesting },
        { provide: Socket, useClass: SocketTesting },
      ],
    })
      .overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [
            RemoveDialogComponent,
            LoadingComponent,
            GraphErrorSnackbarComponent,
          ],
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    TEST_CODE = '2a';
    graphService = TestBed.get(GraphService);
    route = TestBed.get(ActivatedRoute);
    helperService = TestBed.get(HelperService);
    dialog = TestBed.get(MatDialog);
    socket = TestBed.get(Socket);
    router = TestBed.get(Router);
    snackBar = TestBed.get(MatSnackBar);

    fixture = TestBed.createComponent(GraphPageComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init graph when component init', () => {
    graphService.data = null;
    graphService.data = graphStubData();
    const spyLoadGraph = spyOn(graphService, 'loadGraph').and.returnValue(of(graphService.data));
    const spyParams = spyOnProperty(route, 'params', 'get').and.returnValue(of({ code: '2a' }));
    const spySnackBar = spyOn(helperService, 'openErrorSnackBar');
    fixture.detectChanges();
    expect(spyParams).toHaveBeenCalled();
    spyParams.calls.reset();
    expect(spyLoadGraph).toHaveBeenCalled();
    spyLoadGraph.calls.reset();
    expect(graphService.data).toBeTruthy();

    const errorMessageStatusText = 'DB ERROR status text';
    spyLoadGraph.and.returnValue(throwError({ statusText: errorMessageStatusText }));
    fixture = TestBed.createComponent(GraphPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(spySnackBar).toHaveBeenCalled();
    let messageArg = spySnackBar.calls.mostRecent().args[0];
    expect(messageArg).toBe(errorMessageStatusText);
    spySnackBar.calls.reset();

    const errorMessageMessage = 'DB ERROR message';
    spyLoadGraph.and.returnValue(throwError({ error: { message: errorMessageMessage } }));
    fixture = TestBed.createComponent(GraphPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(spySnackBar).toHaveBeenCalled();
    messageArg = spySnackBar.calls.mostRecent().args[0];
    expect(messageArg).toBe(errorMessageMessage);
    spySnackBar.calls.reset();
  });

  it('should init new graph data when param "code" is "new"', () => {
    graphService.data = null;
    graphService.data = graphStubData();
    const spyLoadGraph = spyOn(graphService, 'loadGraph').and.returnValue(of(graphService.data));
    const spyParams = spyOnProperty(route, 'params', 'get').and.returnValue(of({ code: 'new' }));
    fixture.detectChanges();
    expect(spyParams).toHaveBeenCalled();
    spyParams.calls.reset();
    expect(spyLoadGraph).not.toHaveBeenCalled();
    spyLoadGraph.calls.reset();
    expect(graphService.data).toBeTruthy();
    expect(graphService.data.code).toBeFalsy();
  });

  it('should actvate edit mode when click on "lock"', () => {
    graphService.data = graphStubData();
    fixture.detectChanges();
    const previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[0].nativeElement.click();
    expect(component.showEditorRootControls).toBe(true);
    expect(component.graphMode).toBe(null);
  });

  it('should actvate preview mode when click on "unlock"', fakeAsync(() => {
    spyOn(graphService, 'loadGraph').and.returnValue(of(graphStubData()));
    fixture.detectChanges();
    tick(10000);
    graphService.newGraph();
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    tick(10000);
    let previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[0].nativeElement.click();
    fixture.detectChanges();
    tick(10000);
    expect(dialog.openDialogs.length).toBeFalsy();
    expect(component.showEditorRootControls).toBe(false);
    expect(component.graphMode).toBe(GraphMode.preview);

    component.setMode(EditViewMode.root);
    graphService.data = graphStubData();
    fixture.detectChanges();
    previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[0].nativeElement.click();
    fixture.detectChanges();
    let dialogRef = dialog.openDialogs[0];
    dialogRef.close();
    tick(10000);
    expect(component.showEditorRootControls).toBe(true);
    expect(component.graphMode).toBe(null);

    previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[0].nativeElement.click();
    fixture.detectChanges();
    dialogRef = dialog.openDialogs[0];
    dialogRef.close(true);
    tick(10000);
    expect(component.showEditorRootControls).toBe(false);
    expect(component.graphMode).toBe(GraphMode.preview);
  }));

  it('should not see "unlock" on "graph/new" page', () => {
    graphService.newGraph();
    component.setMode(EditViewMode.root);
    TEST_CODE = 'new';
    fixture.detectChanges();
    const previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    expect(previewButtons.length).toBe(3);
  });

  it('should actvate shapeNew mode when click on "+"', fakeAsync(() => {
    graphService.data = graphStubData();
    fixture.detectChanges();
    tick(10000);
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    const previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[1].nativeElement.click();
    expect(component.showEditorRootControls).toBe(false);
    expect(component.shapeText).toBe('');
    expect(component.graphMode).toBe(GraphMode.readonly);
  }));

  it('should save Graph when click on "Save"', fakeAsync(() => {
    const spySnackBar = spyOn(snackBar, 'openFromComponent');
    graphService.data = graphStubData();
    fixture.detectChanges();
    tick(10000);
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    tick(10000);
    const previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));

    const spySaveGraph = spyOn(graphService, 'saveGraph').and.returnValue(of(graphService.data));
    const spyNavigate = spyOn(router, 'navigate');
    previewButtons[2].nativeElement.click();
    tick(10000);
    expect(spySaveGraph).toHaveBeenCalled();
    spySaveGraph.calls.reset();
    expect(spyNavigate).toHaveBeenCalled();

    const errorMessageStatusText = 'DB ERROR status text';
    spySaveGraph.and.returnValue(throwError({ statusText: errorMessageStatusText }));
    previewButtons[2].nativeElement.click();
    tick(10000);
    expect(spySnackBar).toHaveBeenCalled();
    let messageArg = spySnackBar.calls.mostRecent().args[1];
    expect(messageArg.data).toEqual([errorMessageStatusText]);
    spySnackBar.calls.reset();
    spySaveGraph.calls.reset();

    const errorMessageMessage = graphErrorStubData();
    spySaveGraph.and.returnValue(throwError({ error: { message: errorMessageMessage } }));
    previewButtons[2].nativeElement.click();
    tick(10000);
    expect(spySnackBar).toHaveBeenCalled();
    messageArg = spySnackBar.calls.mostRecent().args[1];
    expect(messageArg.data).toEqual([
      'title :: title must be longer than or equal to 10 characters',
      'shapes :: 0 :: text :: text must be longer than or equal to 10 characters',
    ]);
    spySnackBar.calls.reset();
    spySaveGraph.calls.reset();

    const errorMessageError = 'DB ERROR';
    spySaveGraph.and.returnValue(throwError({ error: { error: errorMessageError } }));
    previewButtons[2].nativeElement.click();
    tick(10000);
    expect(spySnackBar).toHaveBeenCalled();
    messageArg = spySnackBar.calls.mostRecent().args[1];
    expect(messageArg.data).toEqual([errorMessageError]);
    spySnackBar.calls.reset();
    spySaveGraph.calls.reset();
  }));

  it('should enable edit title when edit mode', fakeAsync(() => {
    graphService.data = graphStubData();
    fixture.detectChanges();
    tick(10000);
    const initTitle = graphService.data.title;
    graphService.data = graphService.data;
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    let previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[3].nativeElement.click();
    fixture.detectChanges();
    let editTitleButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    expect(editTitleButtons.length).toBe(5);
    expect(component.titleEdited).toBe(initTitle);

    editTitleButtons[3].nativeElement.click();
    fixture.detectChanges();
    previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    expect(previewButtons.length).toBe(4);
    expect(component.titleEdited).toBe(null);
    expect(graphService.data.title).toBe(initTitle);

    previewButtons[3].nativeElement.click();
    fixture.detectChanges();
    editTitleButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    expect(editTitleButtons.length).toBe(5);

    component.titleEdited = 'New title';
    const spy = spyOn(socket, 'emit');
    editTitleButtons[4].nativeElement.click();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  }));

  it('should open menu on shape click or on link click when edit mode', () => {
    graphService.data = graphStubData();
    graphService.data = graphService.data;
    fixture.detectChanges();
    let rectTextComponent = fixture.debugElement.query(By.directive(RectTextComponent));
    rectTextComponent.nativeElement.dispatchEvent(new Event('click'));
    expect(component.shapeSelected).toBeFalsy();
    let linkComponent = fixture.debugElement.query(By.directive(LinkComponent));
    linkComponent.nativeElement.dispatchEvent(new Event('click'));
    expect(component.linkSelected).toBeFalsy();

    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    const menuTriggerOpenSpies = fixture.debugElement.queryAll(By.directive(MatMenuTrigger))
      .map((el: DebugElement) => {
        const trigger: MatMenuTrigger = el.injector.get(MatMenuTrigger);
        return spyOn(trigger, 'openMenu');
      });
    expect(menuTriggerOpenSpies.length).toBe(2);
    rectTextComponent = fixture.debugElement.query(By.directive(RectTextComponent));
    rectTextComponent.nativeElement.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(component.shapeSelected).toBeTruthy();
    expect(menuTriggerOpenSpies[0]).toHaveBeenCalled();
    expect(menuTriggerOpenSpies[1]).not.toHaveBeenCalled();
    linkComponent = fixture.debugElement.query(By.directive(LinkComponent));
    linkComponent.nativeElement.dispatchEvent(new Event('click'));
    expect(menuTriggerOpenSpies[1]).toHaveBeenCalled();
  });

  it('should be able to create shape when edit mode', fakeAsync(() => {
    graphService.data = graphStubData();
    fixture.detectChanges();
    tick();
    graphService.data = graphService.data;
    const initShapesLength = graphService.data.shapes.length;
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    const previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[1].nativeElement.click();
    fixture.detectChanges();
    expect(component.shapeText).toBe('');
    expect(component.graphMode).toBe(GraphMode.readonly);

    component.shapeText = 'New shape text';
    const shapeEditorButtons = fixture.debugElement.queryAll(By.css('[mat-flat-button]'));
    expect(shapeEditorButtons.length).toBe(2);
    shapeEditorButtons[1].nativeElement.click();
    fixture.detectChanges();
    expect(graphService.newShapes.length).toBe(1);
    expect(component.graphMode).toBe(GraphMode.readonly);
    expect(component.showNewPositionControls).toBe(true);
    const graphComponent = fixture.debugElement.query(By.css('svg'));
    graphComponent.nativeElement.dispatchEvent(new MouseEvent('mouseup', { clientX: 240, clientY: 435 }));
    socket._emitGraph(graphStubData());
    tick();
    fixture.detectChanges();
    expect(graphService.newShapes.length).toBe(0);
    expect(component.graphMode).toBe(null);
  }));

  it('should be able to cancel creating new shape', fakeAsync(() => {
    graphService.data = graphStubData();
    fixture.detectChanges();
    tick();
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    let previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[1].nativeElement.click();
    fixture.detectChanges();
    let shapeEditorButtons = fixture.debugElement.queryAll(By.css('[mat-flat-button]'));
    shapeEditorButtons[0].nativeElement.click();
    fixture.detectChanges();
    expect(component.shapeText).toBe(null);
    expect(component.graphMode).toBe(null);

    previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    previewButtons[1].nativeElement.click();
    fixture.detectChanges();
    component.shapeText = 'New shape text';
    shapeEditorButtons = fixture.debugElement.queryAll(By.css('[mat-flat-button]'));
    shapeEditorButtons[1].nativeElement.click();
    fixture.detectChanges();
    previewButtons = fixture.debugElement.queryAll(By.css('[mat-icon-button]'));
    expect(previewButtons.length).toBe(1);
    previewButtons[0].nativeElement.click();
    fixture.detectChanges();
    expect(component.shapeText).toBe(null);
    expect(component.graphMode).toBe(null);
  }));

  it('should edit shape text via shape menu', fakeAsync(() => {
    socket._emitGraph(graphStubData());
    fixture.detectChanges();
    tick(10000);
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    const rectTextComponent = fixture.debugElement.query(By.directive(RectTextComponent));
    rectTextComponent.nativeElement.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    tick(10000);
    const shapeSelected = component.shapeSelected;
    const initText = component.shapeSelected.text;
    component.setMode(EditViewMode.shapeEdit);
    fixture.detectChanges();
    tick(10000);
    expect(component.showEditorRootControls).toBe(false);
    expect(component.shapeText).toBe(graphService.data.shapes[0].text);
    expect(component.graphMode).toBe(GraphMode.readonly);

    component.shapeText = 'New shape text';
    const shapeEditorButtons = fixture.debugElement.queryAll(By.css('[mat-flat-button]'));
    expect(shapeEditorButtons.length).toBe(2);
    shapeEditorButtons[1].nativeElement.click();
    fixture.detectChanges();
    tick(10000);
    expect(component.graphMode).toBe(null);
    expect(component.shapeSelected).toBe(null);
  }));

  it('should add link via shape menu', fakeAsync(() => {
    socket._emitGraph(graphStubData());
    fixture.detectChanges();
    tick(10000);
    component.setMode(EditViewMode.root);
    const rectTextComponent = fixture.debugElement.query(By.directive(RectTextComponent));
    rectTextComponent.nativeElement.dispatchEvent(new Event('click'));
    tick(10000);

    component.createLink('dest');
    tick(10000);
    expect(graphService.newLinks.length).toBe(1);
    expect(component.graphMode).toBe(GraphMode.readonly);
    expect(component.showNewLinkControls).toBe(true);

    fixture.detectChanges();
    let rectTextComponents = fixture.debugElement.queryAll(By.directive(RectTextComponent));
    socket._graphSuccess = false;
    socket._exceptionMessage = 'test message';
    rectTextComponents[1].nativeElement.dispatchEvent(new Event('click'));
    tick(10000);
    expect(component.graphMode).toBe(GraphMode.readonly);
    expect(component.showNewLinkControls).toBe(true);

    const spyNewLink = spyOn(graphService, 'newLink').and.returnValue(Promise.resolve(null));
    fixture.detectChanges();
    rectTextComponents = fixture.debugElement.queryAll(By.directive(RectTextComponent));
    rectTextComponents[1].nativeElement.dispatchEvent(new Event('click'));
    tick(10000);
    expect(spyNewLink).toHaveBeenCalled();
    expect(component.graphMode).toBe(null);
    expect(component.showNewLinkControls).toBe(false);
  }));

  it('should remove shape via shape menu', fakeAsync(() => {
    socket._emitGraph(graphStubData());
    fixture.detectChanges();
    tick(10000);
    const spyRemoveShape = spyOn(graphService, 'removeShape');
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    let rectTextElement = fixture.debugElement.query(By.directive(RectTextComponent));
    rectTextElement.nativeElement.dispatchEvent(new Event('click'));
    component.removeShape();
    fixture.detectChanges();
    let dialogRef = dialog.openDialogs[0];
    dialogRef.close(true);
    tick(10000);
    expect(spyRemoveShape).toHaveBeenCalled();

    fixture.detectChanges();
    rectTextElement = fixture.debugElement.query(By.directive(RectTextComponent));
    rectTextElement.nativeElement.dispatchEvent(new Event('click'));
    component.removeShape();
    dialogRef = dialog.openDialogs[0];
    dialogRef.close();
    tick(10000);
    expect(spyRemoveShape).toHaveBeenCalledTimes(1);
  }));

  it('should remove link via link menu', fakeAsync(() => {
    socket._emitGraph(graphStubData());
    fixture.detectChanges();
    tick(10000);
    const spyRemoveLink = spyOn(graphService, 'removeLink');
    component.setMode(EditViewMode.root);
    fixture.detectChanges();
    let linkElement = fixture.debugElement.query(By.directive(LinkComponent));
    linkElement.nativeElement.dispatchEvent(new Event('click'));
    component.removeLink();
    fixture.detectChanges();
    let dialogRef = dialog.openDialogs[0];
    dialogRef.close(true);
    tick(10000);
    expect(spyRemoveLink).toHaveBeenCalled();

    fixture.detectChanges();
    linkElement = fixture.debugElement.query(By.directive(LinkComponent));
    linkElement.nativeElement.dispatchEvent(new Event('click'));
    component.removeLink();
    dialogRef = dialog.openDialogs[0];
    const dialogInstance = dialogRef.componentInstance;
    dialogInstance.cancel();
    tick(10000);
    expect(spyRemoveLink).toHaveBeenCalledTimes(1);
  }));
});

let TEST_CODE = '2a';
class ActivatedRouteTesting {
  get params() {
    return of({ code: TEST_CODE });
  }
}
